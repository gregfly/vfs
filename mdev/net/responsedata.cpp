#include "responsedata.h"

#include "../core/utils.h"

#define RESPONSEDATA_LINES_SEPARATOR "\n"

ResponseData::ResponseData(const char * username, const char * password, const char * command, const char * output, bool isSuccess) : RequestData(username, password, command), pIsSuccess(isSuccess)
{
	this->setOutput(output? output : "");
}

ResponseData::ResponseData(RequestData &other) : RequestData(other.username().c_str(), other.password().c_str(), other.command().c_str())
{
	//
}

ResponseData::~ResponseData()
{
	//
}

size_t ResponseData::size() const
{
	return RequestData::size() + sizeof(this->pOutput) + sizeof(this->pIsSuccess);
}
	
std::string ResponseData::output()
{
	this->pOutput[DEFAULT_OUTPUT_SIZE - 1] = '\0';
	return this->pOutput;
}

void ResponseData::setOutput(const std::string &value)
{
	memset(this->pOutput, '\0', DEFAULT_OUTPUT_SIZE);
	strcpy(this->pOutput, value.c_str());
	this->pOutput[DEFAULT_OUTPUT_SIZE - 1] = '\0';
}

bool ResponseData::isSuccess()
{
	return this->pIsSuccess;
}

void ResponseData::setSuccess(bool value)
{
	this->pIsSuccess = value;
}

std::list<std::string> ResponseData::outputList()
{
	std::list<std::string> list;
	Utils::split(this->output(), RESPONSEDATA_LINES_SEPARATOR, list);
	return list;
}

void ResponseData::setOutputList(const std::list<std::string> &value)
{
	this->setOutput(Utils::join(value, RESPONSEDATA_LINES_SEPARATOR));
}