#ifndef ABSTRACTDATA_H
#define ABSTRACTDATA_H

#include "../core/inc.h"

class AbstractData
{
public:
	explicit AbstractData();
	virtual ~AbstractData();

	virtual void * data() = 0;
	virtual size_t size() const = 0;

	virtual std::string username() = 0;
	virtual void setUsername(const std::string &) = 0;
	
	virtual std::string password() = 0;
	virtual void setPassword(const std::string &) = 0;
	
	virtual std::string command() = 0;
	virtual void setCommand(const std::string &) = 0;

};

#endif