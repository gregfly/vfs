#ifndef NOTIFICATIONDATA_H
#define NOTIFICATIONDATA_H

#include "abstractdata.h"

#define DEFAULT_SIZE 512

class NotificationData : public AbstractData
{
public:
	explicit NotificationData(const char * username = NULL, const char * command = NULL);
	virtual ~NotificationData();

	virtual void * data();
	virtual size_t size() const;

	virtual std::string username();
	virtual void setUsername(const std::string &);
	
	virtual std::string command();
	virtual void setCommand(const std::string &);

private:
	virtual std::string password();
	virtual void setPassword(const std::string &);

	char pUsername[DEFAULT_SIZE];
	char pCommand[DEFAULT_SIZE];

};

#endif