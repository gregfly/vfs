#ifndef RESPONSEDATA_H
#define RESPONSEDATA_H

#include "requestdata.h"

#define DEFAULT_OUTPUT_SIZE DEFAULT_SIZE * 4

class ResponseData : public RequestData
{
private:
	char pOutput[DEFAULT_OUTPUT_SIZE];
	bool pIsSuccess;

public:
	explicit ResponseData(const char * username = NULL, const char * password = NULL, const char * command = NULL, const char * output = NULL, bool isSuccess = false);
	ResponseData(RequestData &);
	virtual ~ResponseData();

	virtual size_t size() const;
	
	virtual std::string output();
	virtual void setOutput(const std::string &);
	
	virtual bool isSuccess();
	virtual void setSuccess(bool);

	virtual std::list<std::string> outputList();
	virtual void setOutputList(const std::list<std::string> &);

};

#endif