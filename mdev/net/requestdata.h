#ifndef REQUESTDATA_H
#define REQUESTDATA_H

#include "abstractdata.h"

#define DEFAULT_SIZE 512

class RequestData : public AbstractData
{
public:
	explicit RequestData(const char * username = NULL, const char * password = NULL, const char * command = NULL);
	virtual ~RequestData();

	virtual void * data();
	virtual size_t size() const;

	virtual std::string username();
	virtual void setUsername(const std::string &);
	
	virtual std::string password();
	virtual void setPassword(const std::string &);
	
	virtual std::string command();
	virtual void setCommand(const std::string &);

private:
	char pUsername[DEFAULT_SIZE];
	char pPassword[DEFAULT_SIZE];
	char pCommand[DEFAULT_SIZE];

};

#endif