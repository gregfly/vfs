#include "notificationdata.h"

NotificationData::NotificationData(const char * username, const char * command) : AbstractData()
{
	this->setUsername(username? username : "");
	this->setCommand(command? command : "");
}

NotificationData::~NotificationData()
{
	//
}

void * NotificationData::data()
{
	return (void *)this->pUsername;
}

size_t NotificationData::size() const
{
	return sizeof(this->pUsername) + sizeof(this->pCommand);
}

std::string NotificationData::username()
{
	this->pUsername[DEFAULT_SIZE - 1] = '\0';
	return this->pUsername;
}

void NotificationData::setUsername(const std::string &value)
{
	memset(this->pUsername, '\0', DEFAULT_SIZE);
	strcpy(this->pUsername, value.c_str());
}

std::string NotificationData::command()
{
	this->pCommand[DEFAULT_SIZE - 1] = '\0';
	return this->pCommand;
}

void NotificationData::setCommand(const std::string &value)
{
	memset(this->pCommand, '\0', DEFAULT_SIZE);
	strcpy(this->pCommand, value.c_str());
}

std::string NotificationData::password()
{
	return "";
}

void NotificationData::setPassword(const std::string &)
{
	//
}