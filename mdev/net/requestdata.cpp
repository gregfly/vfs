#include "requestdata.h"

RequestData::RequestData(const char * username, const char * password, const char * command) : AbstractData()
{
	this->setUsername(username? username : "");
	this->setPassword(password? password : "");
	this->setCommand(command? command : "");
}

RequestData::~RequestData()
{
	//
}

void * RequestData::data()
{
	return (void *)this->pUsername;
}

size_t RequestData::size() const
{
	return sizeof(this->pUsername) + sizeof(this->pPassword) + sizeof(this->pCommand);
}

std::string RequestData::username()
{
	this->pUsername[DEFAULT_SIZE - 1] = '\0';
	return this->pUsername;
}

void RequestData::setUsername(const std::string &value)
{
	memset(this->pUsername, '\0', DEFAULT_SIZE);
	strcpy(this->pUsername, value.c_str());
}

std::string RequestData::password()
{
	this->pPassword[DEFAULT_SIZE - 1] = '\0';
	return this->pPassword;
}

void RequestData::setPassword(const std::string &value)
{
	memset(this->pPassword, '\0', DEFAULT_SIZE);
	strcpy(this->pPassword, value.c_str());
}

std::string RequestData::command()
{
	this->pCommand[DEFAULT_SIZE - 1] = '\0';
	return this->pCommand;
}

void RequestData::setCommand(const std::string &value)
{
	memset(this->pCommand, '\0', DEFAULT_SIZE);
	strcpy(this->pCommand, value.c_str());
}