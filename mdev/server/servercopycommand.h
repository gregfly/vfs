#ifndef SERVERCOPYCOMMAND_H
#define SERVERCOPYCOMMAND_H

#include "../core/abstractcommand.h"

class ServerCopyCommand : public AbstractCommand
{
public:
	explicit ServerCopyCommand();
	virtual ~ServerCopyCommand();

	virtual void resolveParams(const std::list<std::string> &tokens, std::map<std::string, std::string> &params) const;

	virtual void run(const std::map<std::string, std::string> &params, std::list<std::string> &output) const;

};

#endif