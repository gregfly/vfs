#ifndef SERVERUSERIDENTITY_H
#define SERVERUSERIDENTITY_H

#include "../core/inc.h"
#include "../core/useridentity.h"

class Dir;

class ServerUserIdentity : public UserIdentity
{
public:
	explicit ServerUserIdentity(const std::string &username, int id);
	virtual ~ServerUserIdentity();

	virtual int id() const;

	virtual std::string username() const;

	Dir * dir() const;
	void setDir(Dir *);

private:
	int pId;
	std::string pUsername;
	Dir * pDir;

};

#endif