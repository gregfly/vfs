#include "serverprintcommand.h"

#include "servercommandexecutor.h"
#include "serveruseridentity.h"
#include "../core/abstractfilesystem.h"
#include "../core/user.h"
#include "../core/dir.h"
#include "../core/exception.h"

ServerPrintCommand::ServerPrintCommand() : AbstractCommand("print")
{
	//
}

ServerPrintCommand::~ServerPrintCommand()
{
	//
}

void ServerPrintCommand::resolveParams(const std::list<std::string> &tokens, std::map<std::string, std::string> &params) const
{
	//
}

void ServerPrintCommand::run(const std::map<std::string, std::string> &params, std::list<std::string> &output) const
{
	ServerCommandExecutor * executor = dynamic_cast<ServerCommandExecutor *>(this->commandExecutor());
	executor->user()->identity<ServerUserIdentity>()->dir()->fileSystem()->print("", output);
}