#include "connectionthread.h"

#include "serverapplication.h"
#include "servercommandexecutor.h"
#include "notificator.h"
#include "../core/abstractfilesystem.h"
#include "../core/mutex.h"
#include "../core/user.h"
#include "../core/abstractuserfinder.h"
#include "../core/utils.h"
#include "../net/requestdata.h"
#include "../net/responsedata.h"
#include "../net/notificationdata.h"
#include <netlink/socket.h>

ConnectionThread::ConnectionThread(Mutex * mutex, Notificator * notificator) : Thread(), pMutex(mutex), pSocket(NULL), pNotificator(notificator)
{
	assert(mutex != NULL);
}

ConnectionThread::~ConnectionThread()
{
	if (this->pSocket) {
		delete this->pSocket;
		this->pSocket = NULL;
	}
}

NL::Socket * ConnectionThread::socket() const
{
	return this->pSocket;
}

void ConnectionThread::attachSocket(NL::Socket * socket)
{
	assert(this->pSocket == NULL);
	this->pSocket = socket;
}

NL::Socket * ConnectionThread::detachSocket()
{
	NL::Socket * socket = this->pSocket;
	this->pSocket = NULL;
	return socket;
}

void ConnectionThread::run()
{
	ServApplication * app = Application::self<ServApplication>();
	RequestData requestData;

	try {
		while (this->pSocket->read(requestData.data(), requestData.size())) {
			bool isOk;
			
			this->pMutex->lock();
			Utils::println(requestData.username() + " say: " + requestData.command());
			app->commandExecutor()->setRequestData(&requestData);
			std::list<std::string> output = app->commandExecutor()->exec(requestData.command(), &isOk);
			app->commandExecutor()->setRequestData(NULL);
			if (isOk && app->fileSystem()->isChangesPerformed()) {
				NotificationData notificationData(requestData.username().c_str(), requestData.command().c_str());
				this->pNotificator->send(&notificationData, this);
			}
			Utils::println(requestData.username() + (isOk? " exec: yes" : " exec: no"));
			this->pMutex->unlock();

			ResponseData response(requestData);

			response.setOutputList(output);
			response.setSuccess(isOk);

			this->pSocket->send(response.data(), response.size());
		}
	} catch(const NL::Exception &ex) {
		Utils::println(ex.what());
	}
	this->pMutex->lock();
	app->user()->userFinder()->destroy(app->user()->userFinder()->find(requestData.username(), requestData.password()));
	this->pMutex->unlock();
}