#include "serverdeltreecommand.h"

#include "servercommandexecutor.h"
#include "serveruseridentity.h"
#include "../core/user.h"
#include "../core/dir.h"
#include "../core/exception.h"

ServerDelTreeCommand::ServerDelTreeCommand() : AbstractCommand("deltree")
{
	//
}

ServerDelTreeCommand::~ServerDelTreeCommand()
{
	//
}

void ServerDelTreeCommand::resolveParams(const std::list<std::string> &tokens, std::map<std::string, std::string> &params) const
{
	if (tokens.size() != 2) {
		throw Exception("Using: " + this->id() + " path");
	}
	params["path"] = tokens.back();
}

void ServerDelTreeCommand::run(const std::map<std::string, std::string> &params, std::list<std::string> &output) const
{
	ServerCommandExecutor * executor = dynamic_cast<ServerCommandExecutor *>(this->commandExecutor());
	if (executor->user()->identity<ServerUserIdentity>()->dir()->removeFolder(params.at("path"), true)) {
		output.push_back("Directory removed");
	}
}