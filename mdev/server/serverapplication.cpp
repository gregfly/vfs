#include "serverapplication.h"

#include "servercommandexecutor.h"
#include "arrayuserfinder.h"
#include "../core/user.h"
#include "../core/dummyfilesystem.h"
#include "../core/inisettings.h"

ServerApplication::ServerApplication(int argc, char * argv[]) : Application(argc, argv), pCommandExecutor(NULL), pUser(NULL), pFileSystem(NULL), pSettings(NULL)
{
	//
}

ServerApplication::~ServerApplication()
{
	if (this->pCommandExecutor) {
		delete this->pCommandExecutor;
		this->pCommandExecutor = NULL;
	}
	if (this->pUser) {
		delete this->pUser;
		this->pUser = NULL;
	}
	if (this->pFileSystem) {
		delete this->pFileSystem;
		this->pFileSystem = NULL;
	}
	if (this->pSettings) {
		delete this->pSettings;
		this->pSettings = NULL;
	}
}

ServerCommandExecutor * ServerApplication::commandExecutor()
{
	if (!this->pCommandExecutor) {
		this->pCommandExecutor = new ServerCommandExecutor(this->user(), this->fileSystem());
	}
	return this->pCommandExecutor;
}

User * ServerApplication::user()
{
	if (!this->pUser) {
		this->pUser = new User(new ArrayUserFinder());
	}
	return this->pUser;
}

AbstractFileSystem * ServerApplication::fileSystem()
{
	if (!this->pFileSystem) {
		this->pFileSystem = new DummyFileSystem();
	}
	return this->pFileSystem;
}

AbstractSettings * ServerApplication::settings()
{
	if (!this->pSettings) {
		std::string filename = "config.ini";
		const std::list<std::string> &args = this->arguments();
		if (args.size() > 1) {
			std::list<std::string>::const_iterator it = args.begin();
			++it;
			filename = *it;
		}
		this->pSettings = new IniSettings(filename);
	}
	return this->pSettings;
}

int ServerApplication::exec()
{
	return 0;
}