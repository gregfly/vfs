#include "serverconnectcommand.h"

#include "servercommandexecutor.h"
#include "serveruseridentity.h"
#include "../core/user.h"
#include "../core/abstractuserfinder.h"
#include "../core/utils.h"
#include "../net/requestdata.h"
#include "../core/dir.h"

ServerConnectCommand::ServerConnectCommand() : AbstractCommand("connect")
{
	//
}

ServerConnectCommand::~ServerConnectCommand()
{
	//
}

void ServerConnectCommand::resolveParams(const std::list<std::string> &tokens, std::map<std::string, std::string> &params) const
{
	//
}

void ServerConnectCommand::run(const std::map<std::string, std::string> &params, std::list<std::string> &output) const
{
	ServerCommandExecutor * executor = dynamic_cast<ServerCommandExecutor *>(this->commandExecutor());
	executor->requestData()->setPassword(Utils::intToString(executor->user()->id())); // set access token
	executor->user()->identity<ServerUserIdentity>()->setDir(new Dir(executor->fileSystem()));
	output.push_back("Active connections: " + Utils::intToString(executor->user()->userFinder()->sessionCount()));
}