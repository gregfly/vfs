#ifndef SERVERMOVECOMMAND_H
#define SERVERMOVECOMMAND_H

#include "../core/abstractcommand.h"

class ServerMoveCommand : public AbstractCommand
{
public:
	explicit ServerMoveCommand();
	virtual ~ServerMoveCommand();

	virtual void resolveParams(const std::list<std::string> &tokens, std::map<std::string, std::string> &params) const;

	virtual void run(const std::map<std::string, std::string> &params, std::list<std::string> &output) const;

};

#endif