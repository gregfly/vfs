#ifndef SERVERMDCOMMAND_H
#define SERVERMDCOMMAND_H

#include "../core/abstractcommand.h"

class ServerMdCommand : public AbstractCommand
{
public:
	explicit ServerMdCommand();
	virtual ~ServerMdCommand();

	virtual void resolveParams(const std::list<std::string> &tokens, std::map<std::string, std::string> &params) const;

	virtual void run(const std::map<std::string, std::string> &params, std::list<std::string> &output) const;

};

#endif