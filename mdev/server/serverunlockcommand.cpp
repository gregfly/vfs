#include "serverunlockcommand.h"

#include "servercommandexecutor.h"
#include "serveruseridentity.h"
#include "../core/user.h"
#include "../core/dir.h"
#include "../core/exception.h"

ServerUnLockCommand::ServerUnLockCommand() : AbstractCommand("unlock")
{
	//
}

ServerUnLockCommand::~ServerUnLockCommand()
{
	//
}

void ServerUnLockCommand::resolveParams(const std::list<std::string> &tokens, std::map<std::string, std::string> &params) const
{
	if (tokens.size() != 2) {
		throw Exception("Using: " + this->id() + " [path]filename");
	}
	params["path"] = tokens.back();
}

void ServerUnLockCommand::run(const std::map<std::string, std::string> &params, std::list<std::string> &output) const
{
	ServerCommandExecutor * executor = dynamic_cast<ServerCommandExecutor *>(this->commandExecutor());
	if (executor->user()->identity<ServerUserIdentity>()->dir()->unlock(params.at("path"), executor->user()->identity<ServerUserIdentity>()->username())) {
		output.push_back("File unlocked");
	}
}