#ifndef ARRAYUSERFINDER_H
#define ARRAYUSERFINDER_H

#include "../core/abstractuserfinder.h"

class UserIdentity;

class ArrayUserFinder : public AbstractUserFinder
{
public:
	explicit ArrayUserFinder();
	virtual ~ArrayUserFinder();

	UserIdentity * find(const std::string &, const std::string &);
	bool destroy(UserIdentity *);

	int sessionCount() const;

private:
	std::list<UserIdentity *> pIdentities;

};

#endif