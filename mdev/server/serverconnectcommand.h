#ifndef SERVERCONNECTCOMMAND_H
#define SERVERCONNECTCOMMAND_H

#include "../core/abstractcommand.h"

class ServerConnectCommand : public AbstractCommand
{
public:
	explicit ServerConnectCommand();
	virtual ~ServerConnectCommand();

	virtual void resolveParams(const std::list<std::string> &tokens, std::map<std::string, std::string> &params) const;

	virtual void run(const std::map<std::string, std::string> &params, std::list<std::string> &output) const;

};

#endif