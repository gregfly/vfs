#include "serveruseridentity.h"

#include "../core/dir.h"

ServerUserIdentity::ServerUserIdentity(const std::string &username, int id) : UserIdentity(), pUsername(username), pId(id), pDir(NULL)
{
	//
}

ServerUserIdentity::~ServerUserIdentity()
{
	if (this->pDir) {
		delete this->pDir;
		this->pDir = NULL;
	}
}

int ServerUserIdentity::id() const
{
	return this->pId;
}

std::string ServerUserIdentity::username() const
{
	return this->pUsername;
}

Dir * ServerUserIdentity::dir() const
{
	return this->pDir;
}

void ServerUserIdentity::setDir(Dir * value)
{
	if (this->pDir) {
		delete this->pDir;
		this->pDir = NULL;
	}
	this->pDir = value;
}