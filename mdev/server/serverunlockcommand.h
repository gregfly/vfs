#ifndef SERVERUNLOCKCOMMAND_H
#define SERVERUNLOCKCOMMAND_H

#include "../core/abstractcommand.h"

class ServerUnLockCommand : public AbstractCommand
{
public:
	explicit ServerUnLockCommand();
	virtual ~ServerUnLockCommand();

	virtual void resolveParams(const std::list<std::string> &tokens, std::map<std::string, std::string> &params) const;

	virtual void run(const std::map<std::string, std::string> &params, std::list<std::string> &output) const;

};

#endif