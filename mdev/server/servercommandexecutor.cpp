#include "servercommandexecutor.h"

#include "../core/user.h"
#include "../core/exception.h"
#include "../net/abstractdata.h"

ServerCommandExecutor::ServerCommandExecutor(User * user, AbstractFileSystem * fileSystem) : CommandExecutor(), pUser(user), pFileSystem(fileSystem), pRequestData(NULL)
{
	//
}

ServerCommandExecutor::~ServerCommandExecutor()
{
	//
}

AbstractData * ServerCommandExecutor::requestData() const
{
	return this->pRequestData;
}

void ServerCommandExecutor::setRequestData(AbstractData * value)
{
	this->pRequestData = value;
}

User * ServerCommandExecutor::user() const
{
	return this->pUser;
}

AbstractFileSystem * ServerCommandExecutor::fileSystem() const
{
	return this->pFileSystem;
}

bool ServerCommandExecutor::beforeExec(AbstractCommand * cmd)
{
	if (!CommandExecutor::beforeExec(cmd)) {
		return false;
	}
	if (this->pUser->login(this->pRequestData->username(), this->pRequestData->password())) {
		return true;
	}
	throw Exception("Username busy");
}

void ServerCommandExecutor::afterExec(AbstractCommand * cmd, std::list<std::string> &output)
{
	this->pUser->logout(false);
	CommandExecutor::afterExec(cmd, output);
}