#ifndef SERVERDELTREECOMMAND_H
#define SERVERDELTREECOMMAND_H

#include "../core/abstractcommand.h"

class ServerDelTreeCommand : public AbstractCommand
{
public:
	explicit ServerDelTreeCommand();
	virtual ~ServerDelTreeCommand();

	virtual void resolveParams(const std::list<std::string> &tokens, std::map<std::string, std::string> &params) const;

	virtual void run(const std::map<std::string, std::string> &params, std::list<std::string> &output) const;

};

#endif