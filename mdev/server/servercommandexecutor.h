#ifndef SERVERCOMMANDEXECUTOR_H
#define SERVERCOMMANDEXECUTOR_H

#include "../core/inc.h"
#include "../core/commandexecutor.h"

class User;
class AbstractFileSystem;
class AbstractData;

class ServerCommandExecutor : public CommandExecutor
{
public:
	explicit ServerCommandExecutor(User *, AbstractFileSystem *);
	virtual ~ServerCommandExecutor();

	AbstractData * requestData() const;
	void setRequestData(AbstractData *);

	User * user() const;

	AbstractFileSystem * fileSystem() const;
	
protected:
	virtual bool beforeExec(AbstractCommand *);
	virtual void afterExec(AbstractCommand *, std::list<std::string> &output);

	User * pUser;
	AbstractFileSystem * pFileSystem;
	AbstractData * pRequestData;

};

#endif