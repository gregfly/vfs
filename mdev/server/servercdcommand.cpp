#include "servercdcommand.h"

#include "servercommandexecutor.h"
#include "serveruseridentity.h"
#include "../core/user.h"
#include "../core/dir.h"
#include "../core/exception.h"

ServerCdCommand::ServerCdCommand() : AbstractCommand("cd")
{
	//
}

ServerCdCommand::~ServerCdCommand()
{
	//
}

void ServerCdCommand::resolveParams(const std::list<std::string> &tokens, std::map<std::string, std::string> &params) const
{
	if (tokens.size() != 2) {
		throw Exception("Using: " + this->id() + " path");
	}
	params["path"] = tokens.back();
}

void ServerCdCommand::run(const std::map<std::string, std::string> &params, std::list<std::string> &output) const
{
	ServerCommandExecutor * executor = dynamic_cast<ServerCommandExecutor *>(this->commandExecutor());
	if (executor->user()->identity<ServerUserIdentity>()->dir()->cd(params.at("path"))) {
		output.push_back("Directory switch to " + executor->user()->identity<ServerUserIdentity>()->dir()->path());
	} else {
		throw Exception("Invalid path");
	}
}