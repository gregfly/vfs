#ifndef CONNECTIONTHREAD_H
#define CONNECTIONTHREAD_H

#include "../core/thread.h"

namespace NL
{
	class Socket;
}

class Mutex;
class Notificator;

class ConnectionThread : public Thread
{
public:
	explicit ConnectionThread(Mutex * mutex, Notificator * notificator);
	virtual ~ConnectionThread();

	NL::Socket * socket() const;
	void attachSocket(NL::Socket *);
	NL::Socket * detachSocket();

protected:
	void run();

private:
	Mutex * pMutex;
	NL::Socket * pSocket;
	Notificator * pNotificator;

};

#endif