#ifndef SERVERLOCKCOMMAND_H
#define SERVERLOCKCOMMAND_H

#include "../core/abstractcommand.h"

class ServerLockCommand : public AbstractCommand
{
public:
	explicit ServerLockCommand();
	virtual ~ServerLockCommand();

	virtual void resolveParams(const std::list<std::string> &tokens, std::map<std::string, std::string> &params) const;

	virtual void run(const std::map<std::string, std::string> &params, std::list<std::string> &output) const;

};

#endif