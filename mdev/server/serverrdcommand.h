#ifndef SERVERRDCOMMAND_H
#define SERVERRDCOMMAND_H

#include "../core/abstractcommand.h"

class ServerRdCommand : public AbstractCommand
{
public:
	explicit ServerRdCommand();
	virtual ~ServerRdCommand();

	virtual void resolveParams(const std::list<std::string> &tokens, std::map<std::string, std::string> &params) const;

	virtual void run(const std::map<std::string, std::string> &params, std::list<std::string> &output) const;

};

#endif