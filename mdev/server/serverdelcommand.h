#ifndef SERVERDELCOMMAND_H
#define SERVERDELCOMMAND_H

#include "../core/abstractcommand.h"

class ServerDelCommand : public AbstractCommand
{
public:
	explicit ServerDelCommand();
	virtual ~ServerDelCommand();

	virtual void resolveParams(const std::list<std::string> &tokens, std::map<std::string, std::string> &params) const;

	virtual void run(const std::map<std::string, std::string> &params, std::list<std::string> &output) const;

};

#endif