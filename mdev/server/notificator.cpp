#include "notificator.h"

#include "connectionthread.h"
#include "../net/notificationdata.h"
#include <netlink/socket.h>

Notificator::Notificator(std::list<ConnectionThread *> * threads) : pThreads(threads)
{
	//
}

Notificator::~Notificator()
{
	//
}

void Notificator::send(NotificationData * data, ConnectionThread * currentThread)
{
	ConnectionThread * thread = NULL;
	std::list<ConnectionThread *>::iterator it = this->pThreads->begin();
	for (; it != this->pThreads->end(); ++it) {
		thread = *it;
		try {
			if (thread && thread->isRunning() && thread->socket() && thread != currentThread) {
				thread->socket()->send(data->data(), data->size());
			}
		} catch (const NL::Exception &) {
			//
		}
		thread = NULL;
	}
}