#ifndef SERVERMFCOMMAND_H
#define SERVERMFCOMMAND_H

#include "../core/abstractcommand.h"

class ServerMfCommand : public AbstractCommand
{
public:
	explicit ServerMfCommand();
	virtual ~ServerMfCommand();

	virtual void resolveParams(const std::list<std::string> &tokens, std::map<std::string, std::string> &params) const;

	virtual void run(const std::map<std::string, std::string> &params, std::list<std::string> &output) const;

};

#endif