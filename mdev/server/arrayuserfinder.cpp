#include "arrayuserfinder.h"

#include "serveruseridentity.h"
#include "../core/utils.h"
#include "../core/exception.h"

ArrayUserFinder::ArrayUserFinder() : AbstractUserFinder()
{
	//
}

ArrayUserFinder::~ArrayUserFinder()
{
	std::list<UserIdentity *>::iterator it = this->pIdentities.begin();
	for (; it != this->pIdentities.end(); ++it) {
		UserIdentity * tmp = *it;
		delete tmp;
		tmp = NULL;
	}
	this->pIdentities.clear();
}

UserIdentity * ArrayUserFinder::find(const std::string &username, const std::string &key)
{
	if (username.empty()) {
		return NULL;
	}
	UserIdentity * identity = NULL;
	int id = key.empty()? -1 : Utils::stringToInt(key);
	std::list<UserIdentity *>::iterator it = this->pIdentities.begin();
	for (; it != this->pIdentities.end(); ++it) {
		identity = *it;
		if (Utils::compare(dynamic_cast<ServerUserIdentity *>(identity)->username(), username) == 0) {
			return identity->id() == id? identity : NULL;
		}
		identity = NULL;
	}
	identity = new ServerUserIdentity(username, this->pIdentities.size());
	this->pIdentities.push_back(identity);
	return identity;
}

bool ArrayUserFinder::destroy(UserIdentity * storedIdentity)
{
	if (!storedIdentity) {
		return false;
	}
	UserIdentity * identity = NULL;
	std::list<UserIdentity *>::iterator it = this->pIdentities.begin();
	for (; it != this->pIdentities.end(); ++it) {
		identity = *it;
		if (identity->id() == storedIdentity->id()) {
			it = this->pIdentities.erase(it);
			return true;
		}
		identity = NULL;
	}
	return false;
}

int ArrayUserFinder::sessionCount() const
{
	return this->pIdentities.size();
}