#ifndef SERVERPRINTCOMMAND_H
#define SERVERPRINTCOMMAND_H

#include "../core/abstractcommand.h"

class ServerPrintCommand : public AbstractCommand
{
public:
	explicit ServerPrintCommand();
	virtual ~ServerPrintCommand();

	virtual void resolveParams(const std::list<std::string> &tokens, std::map<std::string, std::string> &params) const;

	virtual void run(const std::map<std::string, std::string> &params, std::list<std::string> &output) const;

};

#endif