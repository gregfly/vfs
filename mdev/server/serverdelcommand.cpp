#include "serverdelcommand.h"

#include "servercommandexecutor.h"
#include "serveruseridentity.h"
#include "../core/user.h"
#include "../core/dir.h"
#include "../core/exception.h"

ServerDelCommand::ServerDelCommand() : AbstractCommand("del")
{
	//
}

ServerDelCommand::~ServerDelCommand()
{
	//
}

void ServerDelCommand::resolveParams(const std::list<std::string> &tokens, std::map<std::string, std::string> &params) const
{
	if (tokens.size() != 2) {
		throw Exception("Using: " + this->id() + " [path]filename");
	}
	params["path"] = tokens.back();
}

void ServerDelCommand::run(const std::map<std::string, std::string> &params, std::list<std::string> &output) const
{
	ServerCommandExecutor * executor = dynamic_cast<ServerCommandExecutor *>(this->commandExecutor());
	if (executor->user()->identity<ServerUserIdentity>()->dir()->removeFile(params.at("path"))) {
		output.push_back("File removed");
	}
}