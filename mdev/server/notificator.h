#ifndef NOTIFICATOR_H
#define NOTIFICATOR_H

#include "../core/inc.h"

class NotificationData;
class ConnectionThread;

class Notificator
{
public:
	explicit Notificator(std::list<ConnectionThread *> *);
	virtual ~Notificator();

	void send(NotificationData * data, ConnectionThread * current);

private:
	std::list<ConnectionThread *> * pThreads;

};

#endif