#include "servercopycommand.h"

#include "servercommandexecutor.h"
#include "serveruseridentity.h"
#include "../core/user.h"
#include "../core/dir.h"
#include "../core/exception.h"

ServerCopyCommand::ServerCopyCommand() : AbstractCommand("copy")
{
	//
}

ServerCopyCommand::~ServerCopyCommand()
{
	//
}

void ServerCopyCommand::resolveParams(const std::list<std::string> &tokens, std::map<std::string, std::string> &params) const
{
	if (tokens.size() != 3) {
		throw Exception("Using: " + this->id() + " source destination");
	}
	std::list<std::string>::const_iterator it = tokens.begin();
	++it;
	params["src"] = *it;
	params["dest"] = tokens.back();
}

void ServerCopyCommand::run(const std::map<std::string, std::string> &params, std::list<std::string> &output) const
{
	ServerCommandExecutor * executor = dynamic_cast<ServerCommandExecutor *>(this->commandExecutor());
	if (executor->user()->identity<ServerUserIdentity>()->dir()->copy(params.at("src"), params.at("dest"))) {
		output.push_back("Success");
	}
}