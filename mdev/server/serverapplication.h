#ifndef SERVERAPPLICATION_H
#define SERVERAPPLICATION_H

#include "../core/application.h"

class ServerCommandExecutor;
class User;
class AbstractFileSystem;
class AbstractSettings;

class ServerApplication : public Application
{
public:
	explicit ServerApplication(int argc, char * argv[]);
	virtual ~ServerApplication();

	ServerCommandExecutor * commandExecutor();

	User * user();

	AbstractFileSystem * fileSystem();

	AbstractSettings * settings();

	virtual int exec();

private:
	ServerCommandExecutor * pCommandExecutor;
	User * pUser;
	AbstractFileSystem * pFileSystem;
	AbstractSettings * pSettings;

};

typedef ServerApplication ServApplication; // resolve conflict with netlink

#endif