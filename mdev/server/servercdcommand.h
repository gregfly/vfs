#ifndef SERVERCDCOMMAND_H
#define SERVERCDCOMMAND_H

#include "../core/abstractcommand.h"

class ServerCdCommand : public AbstractCommand
{
public:
	explicit ServerCdCommand();
	virtual ~ServerCdCommand();

	virtual void resolveParams(const std::list<std::string> &tokens, std::map<std::string, std::string> &params) const;

	virtual void run(const std::map<std::string, std::string> &params, std::list<std::string> &output) const;

};

#endif