#include "core/defs.h"

/* CONFIGURATION */
#define BUILD_SERVER

#ifndef BUILD_SERVER
#define BUILD_CLIENT
#else
#ifdef APP_ENV_WINDOWS
#define BUILD_SERVER_WINDOWS
#else
#define BUILD_SERVER_LINUX
#endif
#endif
/* END CONFIGURATION */

/* MAIN DECLARATIONS */
#ifdef BUILD_SERVER_WINDOWS

#include "server/serverapplication.h"
#include "server/servercommandexecutor.h"
#include "server/serverconnectcommand.h"
#include "server/servermdcommand.h"
#include "server/servercdcommand.h"
#include "server/serverrdcommand.h"
#include "server/serverdeltreecommand.h"
#include "server/servermfcommand.h"
#include "server/serverdelcommand.h"
#include "server/serverlockcommand.h"
#include "server/serverunlockcommand.h"
#include "server/servercopycommand.h"
#include "server/servermovecommand.h"
#include "server/serverprintcommand.h"
#include "core/utils.h"
#include <netlink/socket.h>
#include "server/connectionthread.h"
#include "core/mutex.h"
#include "core/abstractsettings.h"
#include "server/notificator.h"

int main(int argc, char * argv[])
{
	ServApplication app(argc, argv);
	std::string listenPort = app.settings()->value("port", Utils::intToString(DEFAULT_TCP_PORT));
	std::list<ConnectionThread *> threads;
	Notificator notificator(&threads);
	Mutex mutex;

	app.commandExecutor()->append(new ServerConnectCommand());
	app.commandExecutor()->append(new ServerMdCommand());
	app.commandExecutor()->append(new ServerCdCommand());
	app.commandExecutor()->append(new ServerRdCommand());
	app.commandExecutor()->append(new ServerDelTreeCommand());
	app.commandExecutor()->append(new ServerMfCommand());
	app.commandExecutor()->append(new ServerDelCommand());
	app.commandExecutor()->append(new ServerLockCommand());
	app.commandExecutor()->append(new ServerUnLockCommand());
	app.commandExecutor()->append(new ServerCopyCommand());
	app.commandExecutor()->append(new ServerMoveCommand());
	app.commandExecutor()->append(new ServerPrintCommand());

	NL::init();
	NL::Socket * clientConnection = NULL;
	NL::Socket server(Utils::stringToInt(listenPort));
	ConnectionThread * thread = NULL;

	Utils::println("Welcome to server!");
	Utils::println("Listen port: " + listenPort);
	Utils::println("Waiting for first connection...");
	while (app.commandExecutor()->isRunning()) {
		try {
			thread = NULL;
			clientConnection = server.accept();
			std::list<ConnectionThread *>::iterator it = threads.begin();
			while (it != threads.end()) {
				if (!(*it)->isRunning()) {
					NL::Socket * tmpSocket = (*it)->detachSocket();
					if (tmpSocket) {
						delete tmpSocket;
						tmpSocket = NULL;
					}
					if (thread) {
						Utils::println("Delete unused thread");
						ConnectionThread * tmpThread = *it;
						threads.erase(it++);
						delete tmpThread;
						tmpThread = NULL;
						continue;
					} else {
						Utils::println("Using existed thread");
						thread = *it;
					}
				}
				++it;
			}
			if (!thread) {
				Utils::println("Create new thread");
				thread = new ConnectionThread(&mutex, &notificator);
				threads.push_back(thread);
			}
			thread->attachSocket(clientConnection);
			Utils::println("Started connection");
			thread->start();
		} catch(const NL::Exception &ex) {
			Utils::println(ex.what());
		}
	}
	std::list<ConnectionThread *>::iterator it = threads.begin();
	for (; it != threads.end(); ++it) {
		ConnectionThread * tmpThread = *it;
		if (tmpThread) {
			delete tmpThread;
			tmpThread = NULL;
		}
	}
	threads.clear();

	return app.exec();
}

#endif
#ifdef BUILD_SERVER_LINUX

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>

int daemonMain(int argc, char * argv[]);

int main(int argc, char * argv[])
{
	pid_t pid, sid;

	pid = fork();
	if (pid < 0) {
		exit(EXIT_FAILURE);
	} else if (pid > 0) {
		exit(EXIT_SUCCESS);
	}

	sid = setsid();
	if (sid < 0) {
		exit(EXIT_FAILURE);
	}
	if ((chdir("/")) < 0) {
		exit(EXIT_FAILURE);
	}

	return daemonMain(int argc, char * argv[]);
}

#include "server/serverapplication.h"
#include "server/servercommandexecutor.h"
#include "server/serverconnectcommand.h"
#include "server/servermdcommand.h"
#include "server/servercdcommand.h"
#include "server/serverrdcommand.h"
#include "server/serverdeltreecommand.h"
#include "server/servermfcommand.h"
#include "server/serverdelcommand.h"
#include "server/serverlockcommand.h"
#include "server/serverunlockcommand.h"
#include "server/servercopycommand.h"
#include "server/servermovecommand.h"
#include "server/serverprintcommand.h"
#include "core/utils.h"
#include <netlink/socket.h>
#include "server/connectionthread.h"
#include "core/mutex.h"
#include "core/abstractsettings.h"
#include "server/notificator.h"

int daemonMain(int argc, char * argv[])
{
	ServApplication app(argc, argv);
	std::string listenPort = app.settings()->value("port", Utils::intToString(DEFAULT_TCP_PORT));
	std::list<ConnectionThread *> threads;
	Notificator notificator(&threads);
	Mutex mutex;

	app.commandExecutor()->append(new ServerConnectCommand());
	app.commandExecutor()->append(new ServerMdCommand());
	app.commandExecutor()->append(new ServerCdCommand());
	app.commandExecutor()->append(new ServerRdCommand());
	app.commandExecutor()->append(new ServerDelTreeCommand());
	app.commandExecutor()->append(new ServerMfCommand());
	app.commandExecutor()->append(new ServerDelCommand());
	app.commandExecutor()->append(new ServerLockCommand());
	app.commandExecutor()->append(new ServerUnLockCommand());
	app.commandExecutor()->append(new ServerCopyCommand());
	app.commandExecutor()->append(new ServerMoveCommand());
	app.commandExecutor()->append(new ServerPrintCommand());

	NL::init();
	NL::Socket * clientConnection = NULL;
	NL::Socket server(Utils::stringToInt(listenPort));
	ConnectionThread * thread = NULL;

	Utils::println("Welcome to server!");
	Utils::println("Listen port: " + listenPort);
	Utils::println("Waiting for first connection...");
	while (app.commandExecutor()->isRunning()) {
		try {
			thread = NULL;
			clientConnection = server.accept();
			std::list<ConnectionThread *>::iterator it = threads.begin();
			while (it != threads.end()) {
				if (!(*it)->isRunning()) {
					NL::Socket * tmpSocket = (*it)->detachSocket();
					if (tmpSocket) {
						delete tmpSocket;
						tmpSocket = NULL;
					}
					if (thread) {
						Utils::println("Delete unused thread");
						ConnectionThread * tmpThread = *it;
						threads.erase(it++);
						delete tmpThread;
						tmpThread = NULL;
						continue;
					} else {
						Utils::println("Using existed thread");
						thread = *it;
					}
				}
				++it;
			}
			if (!thread) {
				Utils::println("Create new thread");
				thread = new ConnectionThread(&mutex, &notificator);
				threads.push_back(thread);
			}
			thread->attachSocket(clientConnection);
			Utils::println("Started connection");
			thread->start();
		} catch(const NL::Exception &ex) {
			Utils::println(ex.what());
		}
	}
	std::list<ConnectionThread *>::iterator it = threads.begin();
	for (; it != threads.end(); ++it) {
		ConnectionThread * tmpThread = *it;
		if (tmpThread) {
			delete tmpThread;
			tmpThread = NULL;
		}
	}
	threads.clear();

	return app.exec();
}

#endif
#ifdef BUILD_CLIENT

#include "client/clientapplication.h"
#include "client/clientcommandexecutor.h"
#include "client/clientconnectcommand.h"
#include "client/clientquitcommand.h"
#include "client/clientremotecommand.h"
#include "client/notificationthread.h"
#include "core/mutex.h"
#include "core/defs.h"
#include "core/utils.h"
#include <iostream>

int main(int argc, char * argv[])
{
	ClientApplication app(argc, argv);
	Mutex mutex;
	NotificationThread thread(&mutex, 500);

	app.commandExecutor()->append(new ClientConnectCommand(DEFAULT_TCP_PORT));
	app.commandExecutor()->append(new ClientQuitCommand());
	app.commandExecutor()->append(new ClientRemoteCommand());

	char line[256];

	Utils::println("Welcome!");
	Utils::println("Type to connect : connect host[:port] username");
	while (app.commandExecutor()->isRunning()) {
		memset(line, '\0', 256);
		std::cin.clear();
		std::cin.getline(line, 255);
		const std::list<std::string> &output = app.commandExecutor()->exec(line);
		if (!app.commandExecutor()->socket()) {
			thread.stop();
			thread.setSocket(NULL);
		} else if (!thread.isRunning()) {
			thread.setSocket(app.commandExecutor()->socket());
			thread.start();
		}
		std::list<std::string>::const_iterator it = output.begin();
		for (; it != output.end(); ++it) {
			Utils::println(*it);
		}
		mutex.unlock();
	}
	thread.stop();

	return app.exec();
}

#endif
/* END MAIN DECLARATION */