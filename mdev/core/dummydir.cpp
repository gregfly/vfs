#include "dummydir.h"

#include "abstractfilesystem.h"
#include "dummyfile.h"
#include "utils.h"

DummyDir::DummyDir(const std::string &name, DummyDir * parent, AbstractFileSystem * fileSystem) : AbstractDir(fileSystem), pName(name), pParent(parent)
{
	//
}

DummyDir::DummyDir(const DummyDir &other) : AbstractDir(other.fileSystem()), pName(other.name()), pParent(other.parent())
{
	std::list<DummyDir *>::const_iterator it = other.pDirectories.begin();
	for (; it != other.pDirectories.end(); ++it) {
		this->append(new DummyDir(**it));
	}
	std::list<DummyFile *>::const_iterator it2 = other.pFiles.begin();
	for (; it2 != other.pFiles.end(); ++it2) {
		this->append(new DummyFile(**it2));
	}
}

DummyDir::~DummyDir()
{
	std::list<DummyDir *>::const_iterator it = this->pDirectories.begin();
	for (; it != this->pDirectories.end(); ++it) {
		DummyDir * d = *it;
		delete d;
		d = NULL;
	}
	std::list<DummyFile *>::const_iterator it2 = this->pFiles.begin();
	for (; it2 != this->pFiles.end(); ++it2) {
		DummyFile * d = *it2;
		delete d;
		d = NULL;
	}
	this->pDirectories.clear();
	this->pFiles.clear();
}

DummyDir * DummyDir::parent() const
{
	return this->pParent;
}

void DummyDir::setParent(DummyDir * value)
{
	this->pParent = value;
}

const std::string &DummyDir::name() const
{
	return this->pName;
}

void DummyDir::setName(const std::string &value)
{
	this->pName = value;
}

const std::string DummyDir::path() const
{
	std::list<std::string> parts;
	parts.push_front(this->pName);
	DummyDir * parent = this->pParent;
	while (parent) {
		parts.push_front(parent->name());
		parent = parent->parent();
	}
	return this->fileSystem()->normalizePath(Utils::join(parts, this->fileSystem()->separator()));
}

const std::list<DummyDir *> &DummyDir::directories() const
{
	return this->pDirectories;
}

const std::list<DummyFile *> &DummyDir::files() const
{
	return this->pFiles;
}

const std::list<AbstractFileSystemItem *> DummyDir::childrens() const
{
	std::list<AbstractFileSystemItem *> result;
	std::list<DummyDir *>::const_iterator it = this->pDirectories.begin();
	for (; it != this->pDirectories.end(); ++it) {
		result.push_back(*it);
	}
	std::list<DummyFile *>::const_iterator it2 = this->pFiles.begin();
	for (; it2 != this->pFiles.end(); ++it2) {
		result.push_back(*it2);
	}
	result.sort([] (AbstractFileSystemItem * item1, AbstractFileSystemItem * item2) {
		return Utils::compare(item1->name(), item2->name()) < 0;
	});
	return result;
}

void DummyDir::append(DummyDir * value)
{
	value->setParent(this);
	this->pDirectories.push_back(value);
}

void DummyDir::append(DummyFile * value)
{
	value->setParent(this);
	this->pFiles.push_back(value);
}

DummyDir * DummyDir::take(DummyDir * value)
{
	bool isFound = false;
	std::list<DummyDir *>::const_iterator it = this->pDirectories.begin();
	for (; it != this->pDirectories.end(); ++it) {
		if ((*it) == value) {
			isFound = true;
			break;
		}
	}
	if (isFound) {
		this->pDirectories.remove(value);
		value->setParent(NULL);
	}
	return isFound? value : NULL;
}

DummyFile * DummyDir::take(DummyFile * value)
{
	bool isFound = false;
	std::list<DummyFile *>::const_iterator it = this->pFiles.begin();
	for (; it != this->pFiles.end(); ++it) {
		if ((*it) == value) {
			isFound = true;
			break;
		}
	}
	if (isFound) {
		this->pFiles.remove(value);
		value->setParent(NULL);
	}
	return isFound? value : NULL;
}

bool DummyDir::isLocked() const
{
	std::list<DummyDir *>::const_iterator it = this->pDirectories.begin();
	for (; it != this->pDirectories.end(); ++it) {
		if ((*it)->isLocked()) {
			return true;
		}
	}
	std::list<DummyFile *>::const_iterator it2 = this->pFiles.begin();
	for (; it2 != this->pFiles.end(); ++it2) {
		if ((*it2)->isLocked()) {
			return true;
		}
	}
	return false;
}

AbstractFileSystemItem * DummyDir::find(const std::list<std::string> &tokens) const
{
	std::list<std::string> parts;
	std::list<std::string>::const_iterator it = tokens.begin();
	for (; it != tokens.end(); ++it) {
		parts.push_back(*it);
	}
	return this->internalFind(parts);
}

DummyDir * DummyDir::findDir(const std::list<std::string> &tokens, bool * isFound) const
{
	AbstractFileSystemItem * item = this->find(tokens);
	if (isFound) {
		*isFound = !!item;
	}
	if (item && item->type() == DummyDir::Dir) {
		return dynamic_cast<DummyDir *>(item);
	}
	return NULL;
}

DummyFile * DummyDir::findFile(const std::list<std::string> &tokens, bool * isFound) const
{
	AbstractFileSystemItem * item = this->find(tokens);
	if (isFound) {
		*isFound = !!item;
	}
	if (item && item->type() == DummyDir::File) {
		return dynamic_cast<DummyFile *>(item);
	}
	return NULL;
}

void DummyDir::print(const std::string &prefix, std::list<std::string> &output) const
{
	output.push_back((prefix.empty()? "" : prefix + "_") + (this->name().empty()? this->fileSystem()->separator() : this->name()));
	const std::list<AbstractFileSystemItem *> &childrens = this->childrens();
	std::list<AbstractFileSystemItem *>::const_iterator it = childrens.begin();
	for (; it != childrens.end(); ++it) {
		(*it)->print(prefix.empty()? "|" : prefix + " |", output);
	}
}

AbstractFileSystemItem * DummyDir::internalFind(std::list<std::string> &parts) const
{
	if (Utils::compare(parts.front(), this->name(), true) != 0) {
		return NULL;
	}
	parts.pop_front();
	if (parts.empty()) {
		return const_cast<DummyDir *>(this);
	}
	std::list<DummyDir *>::const_iterator it = this->pDirectories.begin();
	for (; it != this->pDirectories.end(); ++it) {
		AbstractFileSystemItem * item = (*it)->internalFind(parts);
		if (item) {
			return item;
		}
	}
	if (parts.size() != 1) {
		return NULL;
	}
	std::list<DummyFile *>::const_iterator it2 = this->pFiles.begin();
	for (; it2 != this->pFiles.end(); ++it2) {
		if (Utils::compare(parts.front(), (*it2)->name(), true) == 0) {
			return *it2;
		}
	}
	return NULL;
}