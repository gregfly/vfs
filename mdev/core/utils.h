#ifndef UTILS_H
#define UTILS_H

#include "inc.h"

class Utils
{
public:
	static int stringToInt(const std::string &);
	static std::string intToString(int);
	static std::string join(const std::list<std::string> &parts, const std::string &delimiter);
	static void split(const std::string &string, const std::string &delimiter, std::list<std::string> &);
	static int compare(const std::string &, const std::string &, bool ignoreCase = false);
	static bool match(const std::string &subject, const std::string &pattern);
	
	static void print(const std::string &);
	static void println(const std::string &);

	static void sleep(long msec);
	static void pause();

	static bool readAll(const std::string &filename, std::list<std::string> &);
	static bool saveAll(const std::string &filename, const std::list<std::string> &);

private:
	Utils();
};

#endif