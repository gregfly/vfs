#include "abstractcommandexecutor.h"

#include "abstractcommand.h"
#include "exception.h"
#include "utils.h"

AbstractCommandExecutor::AbstractCommandExecutor() : pIsRunning(true)
{
	//
}

AbstractCommandExecutor::~AbstractCommandExecutor()
{
	//
}

bool AbstractCommandExecutor::isRunning() const
{
	return this->pIsRunning;
}

void AbstractCommandExecutor::setRunning(bool value)
{
	this->pIsRunning = value;
}

const std::string &AbstractCommandExecutor::commandLine() const
{
	return this->pCommandLine;
}

const std::list<std::string> AbstractCommandExecutor::exec(const std::string &string, bool * ok)
{
	this->pCommandLine = string;
	std::list<std::string> output;
	std::map<std::string, std::string> params;
	AbstractCommand * cmd = NULL;
	try {
		this->resolveRequest(string, &cmd, params);
		assert(cmd != NULL);
		cmd->setCommandExecutor(this);
		if (this->beforeExec(cmd)) {
			cmd->run(params, output);
			this->afterExec(cmd, output);
			if (ok) {
				*ok = true;
			}
		} else {
			if (ok) {
				*ok = false;
			}
		}
	} catch (const Exception &ex) {
		output.push_back(ex.message());
		if (ok) {
			*ok = false;
		}
	}
	if (cmd) {
		cmd->setCommandExecutor(NULL);
	}
	this->pCommandLine.clear();
	return output;
}

void AbstractCommandExecutor::resolveRequest(const std::string &string, AbstractCommand ** cmd, std::map<std::string, std::string> &params)
{
	if (string.empty()) {
		throw Exception("Invalid command...");
	}
	std::list<std::string> tokens;
	Utils::split(string, " ", tokens);
	if (tokens.empty()) {
		throw Exception("Failed to parsing command...");
	}
	std::list<std::string>::iterator it = tokens.begin();
	*cmd = this->resolveCommand(*it);
	if (*cmd) {
		this->resolveParams(string, *cmd, tokens, params);
	}
}

void AbstractCommandExecutor::resolveParams(const std::string &, const AbstractCommand * cmd, const std::list<std::string> &tokens, std::map<std::string, std::string> &params)
{
	cmd->resolveParams(tokens, params);
}

bool AbstractCommandExecutor::beforeExec(AbstractCommand *)
{
	return true;
}

void AbstractCommandExecutor::afterExec(AbstractCommand *, std::list<std::string> &)
{
	return;
}