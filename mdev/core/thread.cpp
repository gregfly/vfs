#include "thread.h"

#include "inc.h"
#include "utils.h"
#include <pthread.h>

Thread::Thread() : pTid(NULL), pIsRunning(false)
{
	//
}
Thread::~Thread()
{
	if (this->pTid) {
		this->stop();
		delete this->pTid;
		this->pTid = NULL;
	}
	this->pIsRunning = false;
}

bool Thread::isRunning() const
{
	return !!this->pTid && this->pIsRunning;
}

bool Thread::start()
{
	if (this->isRunning()) {
		return false;
	}
	if (this->pTid) {
		delete this->pTid;
		this->pTid = NULL;
	}
	pthread_t * tid = new pthread_t();
	this->pTid = tid;
	return pthread_create(tid, NULL, Thread::internalRun, (void *)this) == 0;
}

bool Thread::stop()
{
	if (!this->isRunning()) {
		return false;
	}
	if (pthread_kill(*static_cast<pthread_t *>(this->pTid), 0) == 0) {
		this->pIsRunning = false;
	}
	return !this->pIsRunning;
}

void Thread::wait()
{
	if (!this->isRunning()) {
		return;
	}
	pthread_join(*static_cast<pthread_t *>(this->pTid), NULL);
	this->pIsRunning = false;
}

void Thread::sleep(long msec)
{
	Utils::sleep(msec);
}

void * Thread::internalRun(void * argv)
{
	Thread * self = static_cast<Thread *>(argv);
	self->pIsRunning = true;
	self->run();
	self->pIsRunning = false;
	return NULL;
}