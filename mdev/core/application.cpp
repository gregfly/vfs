#include "application.h"

#include "utils.h"

Application * Application::instance = NULL;

Application::Application(int argc, char * argv[]) : argc(argc), argv(argv)
{
	assert(Application::instance == NULL);
	Application::instance = this;
	for (int i = 0; i < this->argc; ++i) {
		this->pArguments.push_back(this->argv[i]);
	}
}

Application::~Application()
{
	//
}

const std::list<std::string> &Application::arguments() const
{
	return this->pArguments;
}

int Application::exec()
{
	Utils::pause();
	return 0;
}