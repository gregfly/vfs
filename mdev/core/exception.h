#ifndef EXCEPTION_H
#define EXCEPTION_H

#include "inc.h"

class Exception
{
public:
	explicit Exception(const std::string &);
	virtual ~Exception();

	const std::string &message() const;

private:
	std::string pMessage;

};

#endif