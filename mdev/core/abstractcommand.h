#ifndef ABSTRACTCOMMAND_H
#define ABSTRACTCOMMAND_H

#include "inc.h"

class AbstractCommandExecutor;

class AbstractCommand
{
public:
	explicit AbstractCommand(const std::string &id = "");
	virtual ~AbstractCommand();

	virtual std::string id() const;
	virtual void setId(const std::string &);

	AbstractCommandExecutor * commandExecutor() const;
	void setCommandExecutor(AbstractCommandExecutor *);

	virtual void resolveParams(const std::list<std::string> &tokens, std::map<std::string, std::string> &params) const;

	virtual void run(const std::map<std::string, std::string> &params, std::list<std::string> &output) const = 0;

private:
	AbstractCommandExecutor * pCommandExecutor;
	std::string pId;

};

#endif