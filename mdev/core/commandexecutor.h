#ifndef COMMANDEXECUTOR_H
#define COMMANDEXECUTOR_H

#include "inc.h"
#include "abstractcommandexecutor.h"

class CommandExecutor : public AbstractCommandExecutor
{
public:
	explicit CommandExecutor();
	virtual ~CommandExecutor();

	void append(AbstractCommand *);
	const std::list<AbstractCommand *> &commands() const;

protected:
	virtual AbstractCommand * resolveCommand(const std::string &);

	std::list<AbstractCommand *> pCommands;
};

#endif