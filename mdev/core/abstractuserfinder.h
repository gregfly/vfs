#ifndef ABSTRACTUSERFINDER_H
#define ABSTRACTUSERFINDER_H

#include "inc.h"

class UserIdentity;

class AbstractUserFinder
{
public:
	explicit AbstractUserFinder();
	virtual ~AbstractUserFinder();

	virtual UserIdentity * find(const std::string &, const std::string &) = 0;
	virtual bool destroy(UserIdentity *) = 0;

	virtual int sessionCount() const = 0;

};

#endif