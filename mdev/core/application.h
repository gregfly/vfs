#ifndef APPLICATION_H
#define APPLICATION_H

#include "inc.h"

class Application
{
public:
	explicit Application(int argc, char * argv[]);
	virtual ~Application();

	template<class TApplication>
	static TApplication * self();

	virtual const std::list<std::string> &arguments() const;

	virtual int exec();

private:
	static Application * instance;

	int argc;
	char ** argv;
	std::list<std::string> pArguments;
};

template<class TApplication>
TApplication * Application::self()
{
	return dynamic_cast<TApplication *>(Application::instance);
}

#endif