#ifndef ABSTRACTDIR_H
#define ABSTRACTDIR_H

#include "abstractfilesystemitem.h"

class AbstractDir : public AbstractFileSystemItem
{
public:
	explicit AbstractDir(AbstractFileSystem * fileSystem);
	virtual ~AbstractDir();

};

#endif