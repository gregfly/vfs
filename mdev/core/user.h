#ifndef USER_H
#define USER_H

#include "inc.h"

class AbstractUserFinder;
class UserIdentity;

class User
{
public:
	explicit User(AbstractUserFinder *);
	virtual ~User();

	virtual int id() const;

	virtual bool login(const std::string &, const std::string &);
	virtual bool logout(bool sessionDestroy = true);

	template<class TUserIdentity>
	TUserIdentity * identity() const;
	void setIdentity(UserIdentity *);

	AbstractUserFinder * userFinder() const;

protected:
	UserIdentity * pIdentity;
	AbstractUserFinder * pUserFinder;

};

template<class TUserIdentity>
TUserIdentity * User::identity() const
{
	return static_cast<TUserIdentity *>(this->pIdentity);
}

#endif