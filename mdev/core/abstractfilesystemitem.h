#ifndef ABSTRACTFILESYSTEMITEM_H
#define ABSTRACTFILESYSTEMITEM_H

#include "inc.h"

class AbstractFileSystem;

class AbstractFileSystemItem
{
public:
	enum Type {
		Dir,
		File,
		Drive
	};

	explicit AbstractFileSystemItem(const Type &type, AbstractFileSystem * fileSystem);
	virtual ~AbstractFileSystemItem();

	const Type type() const;

	virtual AbstractFileSystem * fileSystem() const;

	virtual const std::string &name() const = 0;
	virtual const std::string path() const = 0;
	virtual bool isLocked() const = 0;
	virtual void print(const std::string &prefix, std::list<std::string> &output) const = 0;

protected:
	Type pType;
	AbstractFileSystem * pFileSystem;

};

#endif