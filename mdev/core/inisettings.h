#ifndef INISETTINGS_H
#define INISETTINGS_H

#include "abstractsettings.h"

class IniSettings : public AbstractSettings
{
public:
	explicit IniSettings(const std::string &filename);
	virtual ~IniSettings();

	const std::string &filename() const;

	virtual std::string value(const std::string &key, const std::string &defaultValue) const;
	virtual void setValue(const std::string &key, const std::string &value);

	virtual void remove(const std::string &key);

	virtual bool save();

protected:
	void load();

private:
	std::string pFilename;
	std::map<std::string, std::string> pStore;
	bool pIsLoaded;

};

#endif