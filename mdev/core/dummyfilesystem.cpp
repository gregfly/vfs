#include "dummyfilesystem.h"

#include "dummydir.h"
#include "dummyfile.h"
#include "utils.h"
#include "exception.h"

DummyFileSystem::DummyFileSystem() : AbstractFileSystem(), pRootDir(NULL), pIsChangesPerformed(false)
{
	this->initDirectoryTree();
}

DummyFileSystem::~DummyFileSystem()
{
	if (this->pRootDir) {
		delete this->pRootDir;
		this->pRootDir = NULL;
	}
}

bool DummyFileSystem::isChangesPerformed()
{
	bool tmp = this->pIsChangesPerformed;
	this->pIsChangesPerformed = false;
	return tmp;
}

bool DummyFileSystem::exists(const std::string &filename) const
{
	bool ok;
	std::string path = this->normalizePath(filename, &ok);
	if (!ok) {
		return false;
	}
	std::list<std::string> parts;
	Utils::split(path, this->separator(), parts);
	if (parts.size() == 2 && parts.back().empty()) {
		parts.pop_back();
	}
	return !!this->pRootDir->find(parts);
}

const AbstractDir * DummyFileSystem::cd(const std::string &dirname) const
{
	std::string path = this->normalizePath(dirname);
	std::list<std::string> parts;
	Utils::split(path, this->separator(), parts);
	if (parts.size() == 2 && parts.back().empty()) {
		parts.pop_back();
	}
	return this->pRootDir->findDir(parts);
}

const AbstractDir * DummyFileSystem::md(const std::string &dirname)
{
	std::string path = this->normalizePath(dirname);
	std::list<std::string> parts;
	Utils::split(path, this->separator(), parts);
	if (parts.empty()) {
		throw Exception("Invalid path");
	}
	if (this->pRootDir->find(parts)) {
		throw Exception("Invalid path");
	}
	std::string newDirname = parts.back();
	parts.pop_back();
	DummyDir * parentDir = this->pRootDir->findDir(parts);
	if (!parentDir) {
		throw Exception("Folder can't be created");
	}
	DummyDir * newDir = new DummyDir(newDirname, NULL, this);
	parentDir->append(newDir);
	this->pIsChangesPerformed = true;
	return newDir;
}

bool DummyFileSystem::rd(const std::string &dirname, bool recursive)
{
	std::string path = this->normalizePath(dirname);
	std::list<std::string> parts;
	Utils::split(path, this->separator(), parts);
	if (parts.empty()) {
		throw Exception("Invalid path");
	}
	DummyDir * currentDir = this->pRootDir->findDir(parts);
	if (!currentDir) {
		throw Exception("Folder not found");
	}
	if (currentDir == this->pRootDir || (!recursive && !currentDir->directories().empty())) {
		throw Exception("Invalid path");
	}
	if (currentDir->isLocked()) {
		throw Exception("Folder is locked");
	}
	if (currentDir->parent()->take(currentDir)) {
		delete currentDir;
		currentDir = NULL;
		this->pIsChangesPerformed = true;
		return true;
	}
	return false;
}

const AbstractFile * DummyFileSystem::mf(const std::string &filename)
{
	std::string path = this->normalizePath(filename);
	std::list<std::string> parts;
	Utils::split(path, this->separator(), parts);
	if (parts.empty()) {
		throw Exception("Invalid filename");
	}
	if (this->pRootDir->find(parts)) {
		throw Exception("Invalid path");
	}
	std::string newFilename = parts.back();
	parts.pop_back();
	DummyDir * parentDir = this->pRootDir->findDir(parts);
	if (!parentDir) {
		throw Exception("Folder can't be created");
	}
	DummyFile * newFile = new DummyFile(newFilename, NULL, this);
	parentDir->append(newFile);
	this->pIsChangesPerformed = true;
	return newFile;
}

bool DummyFileSystem::del(const std::string &filename)
{
	std::string path = this->normalizePath(filename);
	std::list<std::string> parts;
	Utils::split(path, this->separator(), parts);
	if (parts.empty()) {
		throw Exception("Invalid path");
	}
	DummyFile * file = this->pRootDir->findFile(parts);
	if (!file) {
		throw Exception("File not found");
	}
	if (file->isLocked()) {
		throw Exception("File is locked");
	}
	if (file->parent()->take(file)) {
		delete file;
		file = NULL;
		this->pIsChangesPerformed = true;
		return true;
	}
	return false;
}

bool DummyFileSystem::lock(const std::string &filename, const std::string &key)
{
	std::string path = this->normalizePath(filename);
	std::list<std::string> parts;
	Utils::split(path, this->separator(), parts);
	if (parts.empty()) {
		throw Exception("Invalid path");
	}
	DummyFile * file = this->pRootDir->findFile(parts);
	if (!file) {
		throw Exception("File not found");
	}
	file->setLocked(key, true);
	return true;
}

bool DummyFileSystem::unlock(const std::string &filename, const std::string &key)
{
	std::string path = this->normalizePath(filename);
	std::list<std::string> parts;
	Utils::split(path, this->separator(), parts);
	if (parts.empty()) {
		throw Exception("Invalid path");
	}
	DummyFile * file = this->pRootDir->findFile(parts);
	if (!file) {
		throw Exception("File not found");
	}
	file->setLocked(key, false);
	return true;
}

bool DummyFileSystem::copy(const std::string &source, const std::string &destination)
{
	std::string sourcePath = this->normalizePath(source);
	std::string destinationPath = this->normalizePath(destination);
	std::list<std::string> parts, destParts;
	Utils::split(sourcePath, this->separator(), parts);
	Utils::split(destinationPath, this->separator(), destParts);
	if (parts.size() == 2 && parts.back().empty()) {
		parts.pop_back();
	}
	if (parts.empty()) {
		throw Exception("Invalid source path");
	}
	if (destParts.size() == 2 && destParts.back().empty()) {
		parts.pop_back();
	}
	if (destParts.empty()) {
		throw Exception("Invalid destination path");
	}
	DummyDir * dir = this->pRootDir->findDir(parts);
	DummyFile * file = this->pRootDir->findFile(parts);
	if (!dir && !file) {
		throw Exception("Invalid source path");
	}
	DummyDir * destDir = this->pRootDir->findDir(destParts);
	if (!destDir) {
		throw Exception("Invalid destination path");
	}
	if (dir) {
		destDir->append(new DummyDir(*dir));
	} else if (file) {
		destDir->append(new DummyFile(*file));
	} else {
		return false;
	}
	this->pIsChangesPerformed = true;
	return true;
}

bool DummyFileSystem::move(const std::string &source, const std::string &destination)
{
	std::string sourcePath = this->normalizePath(source);
	std::string destinationPath = this->normalizePath(destination);
	std::list<std::string> parts, destParts;
	Utils::split(sourcePath, this->separator(), parts);
	Utils::split(destinationPath, this->separator(), destParts);
	if (parts.size() == 2 && parts.back().empty()) {
		parts.pop_back();
	}
	if (parts.empty()) {
		throw Exception("Invalid source path");
	}
	if (destParts.size() == 2 && destParts.back().empty()) {
		parts.pop_back();
	}
	if (destParts.empty()) {
		throw Exception("Invalid destination path");
	}
	DummyDir * dir = this->pRootDir->findDir(parts);
	DummyFile * file = this->pRootDir->findFile(parts);
	if ((!dir && !file) || dir == this->pRootDir) {
		throw Exception("Invalid source path");
	}
	AbstractFileSystemItem * item = NULL;
	if (dir) {
		item = dir;
	} else if (file) {
		item = file;
	}
	if (!item || item->isLocked()) {
		throw Exception("Source is locked");
	}
	DummyDir * destDir = this->pRootDir->findDir(destParts);
	if (!destDir) {
		throw Exception("Invalid destination path");
	}
	if (dir && dir->parent()->take(dir)) {
		destDir->append(dir);
	} else if (file && file->parent()->take(file)) {
		destDir->append(file);
	} else {
		return false;
	}
	this->pIsChangesPerformed = true;
	return true;
}

void DummyFileSystem::print(const std::string &dirname, std::list<std::string> &output)
{
	std::string path = this->normalizePath(dirname);
	std::list<std::string> parts;
	Utils::split(path, this->separator(), parts);
	if (parts.size() == 2 && parts.back().empty()) {
		parts.pop_back();
	}
	if (parts.empty()) {
		throw Exception("Invalid path");
	}
	DummyDir * currentDir = this->pRootDir->findDir(parts);
	if (!currentDir) {
		throw Exception("Folder not found");
	}
	currentDir->print("", output);
}

bool DummyFileSystem::isRelativePath(const std::string &path) const
{
	std::string sep = this->separator();
	return path.empty() || (Utils::compare(path.substr(0, sep.length()), sep, true) != 0);
}

std::string DummyFileSystem::normalizePath(const std::string &path, bool * ok) const
{
	if (ok) {
		*ok = true;
	}
	std::list<std::string> parts;
	std::list<std::string> tokens;
	Utils::split(path, this->separator(), tokens);
	std::list<std::string>::iterator it = tokens.begin();
	for (; it != tokens.end(); ++it) {
		if (Utils::compare(*it, "..") == 0) {
			if (parts.empty()) {
				if (ok) {
					*ok = false;
				}
				return "";
			}
			parts.pop_back();
			continue;
		}
		if (Utils::compare(*it, ".") == 0) {
			continue;
		}
		if (it->empty()) {
			continue;
		}
		parts.push_back(*it);
	}
	return this->separator() + Utils::join(parts, this->separator());
}

void DummyFileSystem::initDirectoryTree()
{
	if (this->pRootDir) {
		delete this->pRootDir;
		this->pRootDir = NULL;
	}
	this->pRootDir = new DummyDir("", NULL, this);
	DummyDir * dir1 = new DummyDir("dir1", NULL, this);
	DummyDir * dir4 = new DummyDir("dir4", NULL, this);
	DummyDir * dir5 = new DummyDir("dir5", NULL, this);

	dir4->append(new DummyDir("dir3", NULL, this));
	dir4->append(new DummyFile("1.txt", NULL, this));
	dir5->append(new DummyFile("2.txt", NULL, this));
	dir5->append(new DummyFile("3.txt", NULL, this));
	dir1->append(new DummyDir("dir2", NULL, this));
	dir1->append(dir4);
	dir1->append(dir5);
	this->pRootDir->append(dir1);
}