#include "abstractcommand.h"

#include "utils.h"

AbstractCommand::AbstractCommand(const std::string &id) : pCommandExecutor(NULL), pId(id)
{
	//
}

AbstractCommand::~AbstractCommand()
{
	//
}

std::string AbstractCommand::id() const
{
	return this->pId;
}

void AbstractCommand::setId(const std::string &value)
{
	this->pId = value;
}

AbstractCommandExecutor * AbstractCommand::commandExecutor() const
{
	return this->pCommandExecutor;
}

void AbstractCommand::setCommandExecutor(AbstractCommandExecutor * value)
{
	this->pCommandExecutor = value;
}

void AbstractCommand::resolveParams(const std::list<std::string> &tokens, std::map<std::string, std::string> &params) const
{
	std::list<std::string>::const_iterator it = tokens.begin();
	int i = 0;
	std::list<std::string> ps;
	while (++it != tokens.end()) {
		Utils::split(*it, "=", ps);
		switch (ps.size()) {
		case 1: {
			params.insert(std::pair<std::string, std::string>(Utils::intToString(i), ps.front()));
			break;
		}
		case 2: {
			params.insert(std::pair<std::string, std::string>(ps.front(), ps.back()));
			break;
		}
		}
		++i;
	}
}