#ifndef ABSTRACTFILE_H
#define ABSTRACTFILE_H

#include "abstractfilesystemitem.h"

class AbstractFile : public AbstractFileSystemItem
{
public:
	explicit AbstractFile(AbstractFileSystem * fileSystem);
	virtual ~AbstractFile();

};

#endif