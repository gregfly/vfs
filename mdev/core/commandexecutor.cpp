#include "commandexecutor.h"

#include "abstractcommand.h"
#include "exception.h"
#include "utils.h"

CommandExecutor::CommandExecutor() : AbstractCommandExecutor()
{
	//
}

CommandExecutor::~CommandExecutor()
{
	std::list<AbstractCommand *>::iterator it = this->pCommands.begin();
	for (; it != this->pCommands.end(); ++it) {
		AbstractCommand * cmd = *it;
		delete cmd;
		cmd = NULL;
	}
	this->pCommands.clear();
}

void CommandExecutor::append(AbstractCommand * cmd)
{
	this->pCommands.push_back(cmd);
}

const std::list<AbstractCommand *> &CommandExecutor::commands() const
{
	return this->pCommands;
}

AbstractCommand * CommandExecutor::resolveCommand(const std::string &name)
{
	std::list<AbstractCommand *>::iterator it = this->pCommands.begin();
	for (; it != this->pCommands.end(); ++it) {
		AbstractCommand * cmd = *it;
		if (Utils::compare(cmd->id(), name, true) == 0) {
			return cmd;
		}
	}
	throw Exception("Command " + name + " not found...");
}