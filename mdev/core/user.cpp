#include "user.h"

#include "abstractuserfinder.h"
#include "useridentity.h"

User::User(AbstractUserFinder * userFinder) : pIdentity(NULL), pUserFinder(userFinder)
{
	//
}

User::~User()
{
	if (this->pUserFinder) {
		delete this->pUserFinder;
		this->pUserFinder = NULL;
	}
}

int User::id() const
{
	return this->pIdentity? this->pIdentity->id() : 0;
}

bool User::login(const std::string &name, const std::string &password)
{
	assert(this->pUserFinder != NULL);
	UserIdentity * identity = this->pUserFinder->find(name, password);
	this->setIdentity(identity);
	return !!identity;
}

bool User::logout(bool sessionDestroy)
{
	if (sessionDestroy) {
		return this->pUserFinder->destroy(this->pIdentity);
	}
	this->setIdentity(NULL);
	return true;
}

void User::setIdentity(UserIdentity * value)
{
	this->pIdentity = value;
}

AbstractUserFinder * User::userFinder() const
{
	return this->pUserFinder;
}