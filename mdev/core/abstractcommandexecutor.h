#ifndef ABSTRACTCOMMANDEXECUTOR_H
#define ABSTRACTCOMMANDEXECUTOR_H

#include "inc.h"

class AbstractCommand;

class AbstractCommandExecutor
{
public:
	explicit AbstractCommandExecutor();
	virtual ~AbstractCommandExecutor();

	virtual bool isRunning() const;
	virtual void setRunning(bool);

	const std::string &commandLine() const;

	virtual const std::list<std::string> exec(const std::string &, bool * ok = NULL);

protected:
	virtual void resolveRequest(const std::string &, AbstractCommand **, std::map<std::string, std::string> &params);
	virtual void resolveParams(const std::string &, const AbstractCommand *, const std::list<std::string> &tokens, std::map<std::string, std::string> &params);
	virtual AbstractCommand * resolveCommand(const std::string &) = 0;

	virtual bool beforeExec(AbstractCommand *);
	virtual void afterExec(AbstractCommand *, std::list<std::string> &output);

	std::string pCommandLine;
	bool pIsRunning;

};

#endif