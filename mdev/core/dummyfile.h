#ifndef DUMMYFILE_H
#define DUMMYFILE_H

#include "abstractfile.h"

class DummyDir;

class DummyFile : public AbstractFile
{
public:
	explicit DummyFile(const std::string &name, DummyDir * parent, AbstractFileSystem * fileSystem);
	DummyFile(const DummyFile &other);
	virtual ~DummyFile();

	DummyDir * parent() const;
	void setParent(DummyDir *);

	const std::string &name() const;
	void setName(const std::string &);

	const std::string path() const;

	bool isLocked() const;
	void setLocked(const std::string &key, bool enable = true);

	void print(const std::string &prefix, std::list<std::string> &) const;

protected:
	std::string pName;
	DummyDir * pParent;
	std::list<std::string> pLockers;

};

#endif