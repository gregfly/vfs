#ifndef DEFS_H
#define DEFS_H

#if defined(_WIN32) || defined(__WIN32__) || defined(_MSC_VER)
#define APP_ENV_WINDOWS
#else
#define APP_ENV_LINUX
#endif

#define pApp(TApplication) Application::self<TApplication>()

#define DEFAULT_TCP_PORT 9000

#endif