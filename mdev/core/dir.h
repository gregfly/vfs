#ifndef DIR_H
#define DIR_H

#include "inc.h"

class AbstractFileSystem;

class Dir
{
public:
	explicit Dir(AbstractFileSystem * fileSystem, const std::string &path = "");
	virtual ~Dir();

	AbstractFileSystem * fileSystem() const;

	const std::string &path() const;
	void setPath(const std::string &);

	bool exists(const std::string &path = "") const;
	bool cd(const std::string &);
	inline bool cdUp() { return this->cd(".."); }
	bool createFolder(const std::string &);
	bool removeFolder(const std::string &, bool recursive = false);
	bool createFile(const std::string &);
	bool removeFile(const std::string &);
	bool lock(const std::string &, const std::string &key);
	bool unlock(const std::string &, const std::string &key);
	bool copy(const std::string &src, const std::string &dest);
	bool move(const std::string &src, const std::string &dest);

protected:
	AbstractFileSystem * pFileSystem;
	std::string pPath;

	std::string normalizePath(const std::string &path) const;

};

#endif