#ifndef USERIDENTITY_H
#define USERIDENTITY_H

class UserIdentity
{
public:
	explicit UserIdentity();
	virtual ~UserIdentity();

	virtual int id() const = 0;

};

#endif