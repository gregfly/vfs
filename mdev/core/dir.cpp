#include "dir.h"

#include "abstractfilesystem.h"
#include "abstractdir.h"
#include "utils.h"
#include "exception.h"

Dir::Dir(AbstractFileSystem * fileSystem, const std::string &path) : pFileSystem(fileSystem)
{
	assert(fileSystem != NULL);
	this->setPath(path);
}

Dir::~Dir()
{
	this->pPath.clear();
	this->pFileSystem = NULL;
}

AbstractFileSystem * Dir::fileSystem() const
{
	return this->pFileSystem;
}

const std::string &Dir::path() const
{
	return this->pPath;
}

void Dir::setPath(const std::string &value)
{
	this->pPath = this->pFileSystem->normalizePath(value);
}

bool Dir::exists(const std::string &filename) const
{
	return this->pFileSystem->exists(this->normalizePath(filename));
}

bool Dir::cd(const std::string &path)
{
	const AbstractDir * currentDir = this->pFileSystem->cd(this->normalizePath(path));
	if (currentDir) {
		this->setPath(currentDir->path());
		return true;
	}
	return false;
}

bool Dir::createFolder(const std::string &path)
{
	return !!this->pFileSystem->md(this->normalizePath(path));
}

bool Dir::removeFolder(const std::string &path, bool recursive)
{
	std::string normalPath = this->normalizePath(path);
	if (Utils::compare(this->pFileSystem->normalizePath(normalPath), this->path()) == 0) {
		throw Exception("Current folder can't be removed");
	}
	return this->pFileSystem->rd(normalPath, recursive);
}

bool Dir::createFile(const std::string &path)
{
	return !!this->pFileSystem->mf(this->normalizePath(path));
}

bool Dir::removeFile(const std::string &path)
{
	return this->pFileSystem->del(this->normalizePath(path));
}

bool Dir::lock(const std::string &path, const std::string &key)
{
	return this->pFileSystem->lock(this->normalizePath(path), key);
}

bool Dir::unlock(const std::string &path, const std::string &key)
{
	return this->pFileSystem->unlock(this->normalizePath(path), key);
}

bool Dir::copy(const std::string &src, const std::string &dest)
{
	return this->pFileSystem->copy(this->normalizePath(src), this->normalizePath(dest));
}

bool Dir::move(const std::string &src, const std::string &dest)
{
	return this->pFileSystem->move(this->normalizePath(src), this->normalizePath(dest));
}

std::string Dir::normalizePath(const std::string &path) const
{
	if (path.empty()) {
		return this->pPath;
	}
	return this->pFileSystem->isRelativePath(path)? this->pPath + this->pFileSystem->separator() + path : path;
}