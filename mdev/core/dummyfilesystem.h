#ifndef DUMMYFILESYSTEM_H
#define DUMMYFILESYSTEM_H

#include "abstractfilesystem.h"

class DummyDir;

class DummyFileSystem : public AbstractFileSystem
{
public:
	explicit DummyFileSystem();
	virtual ~DummyFileSystem();

	virtual bool isChangesPerformed();

	virtual bool exists(const std::string &filename) const;
	virtual const AbstractDir * cd(const std::string &dirname) const;
	virtual const AbstractDir * md(const std::string &dirname);
	virtual bool rd(const std::string &dirname, bool recursive = false);
	virtual const AbstractFile * mf(const std::string &filename);
	virtual bool del(const std::string &filename);
	virtual bool lock(const std::string &filename, const std::string &key);
	virtual bool unlock(const std::string &filename, const std::string &key);
	virtual bool copy(const std::string &source, const std::string &destination);
	virtual bool move(const std::string &source, const std::string &destination);
	virtual void print(const std::string &dirname, std::list<std::string> &output);

	virtual bool isRelativePath(const std::string &path) const;
	virtual std::string normalizePath(const std::string &path, bool * ok = NULL) const;

protected:
	DummyDir * pRootDir;
	bool pIsChangesPerformed;

	void initDirectoryTree();

};

#endif