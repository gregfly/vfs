#ifndef DUMMYDIR_H
#define DUMMYDIR_H

#include "abstractdir.h"

class DummyFile;

class DummyDir : public AbstractDir
{
public:
	explicit DummyDir(const std::string &name, DummyDir * parent, AbstractFileSystem * fileSystem);
	DummyDir(const DummyDir &other);
	virtual ~DummyDir();

	DummyDir * parent() const;
	void setParent(DummyDir *);

	const std::string &name() const;
	void setName(const std::string &);

	const std::string path() const;

	const std::list<DummyDir *> &directories() const;
	const std::list<DummyFile *> &files() const;
	const std::list<AbstractFileSystemItem *> childrens() const;

	void append(DummyDir *);
	void append(DummyFile *);
	DummyDir * take(DummyDir *);
	DummyFile * take(DummyFile *);

	bool isLocked() const;

	AbstractFileSystemItem * find(const std::list<std::string> &) const;
	DummyDir * findDir(const std::list<std::string> &, bool * isFound = NULL) const;
	DummyFile * findFile(const std::list<std::string> &, bool * isFound = NULL) const;

	void print(const std::string &prefix, std::list<std::string> &) const;

protected:
	std::string pName;
	DummyDir * pParent;
	std::list<DummyDir *> pDirectories;
	std::list<DummyFile *> pFiles;

	AbstractFileSystemItem * internalFind(std::list<std::string> &parts) const;

};

#endif