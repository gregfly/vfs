#ifndef ABSTRACTSETTINGS_H
#define ABSTRACTSETTINGS_H

#include "inc.h"

class AbstractSettings
{
public:
	explicit AbstractSettings();
	virtual ~AbstractSettings();

	virtual std::string value(const std::string &key, const std::string &defaultValue) const = 0;
	virtual void setValue(const std::string &key, const std::string &value) = 0;

	virtual void remove(const std::string &key) = 0;

	virtual bool save() = 0;

};

#endif