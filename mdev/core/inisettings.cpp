#include "inisettings.h"

#include "utils.h"

IniSettings::IniSettings(const std::string &filename) : AbstractSettings(), pFilename(filename), pIsLoaded(false)
{
	this->load();
}

IniSettings::~IniSettings()
{
	this->save();
	this->pStore.clear();
}

const std::string &IniSettings::filename() const
{
	return this->pFilename;
}

std::string IniSettings::value(const std::string &key, const std::string &defaultValue) const
{
	std::map<std::string, std::string>::const_iterator it = this->pStore.find(key);
	if (it != this->pStore.end()) {
		return it->second;
	}
	return defaultValue;
}

void IniSettings::setValue(const std::string &key, const std::string &value)
{
	this->pStore[key] = value;
}

void IniSettings::remove(const std::string &key)
{
	this->pStore.erase(key);
}

bool IniSettings::save()
{
	std::list<std::string> lines;
	std::map<std::string, std::string>::const_iterator it = this->pStore.begin();
	for (; it != this->pStore.end(); ++it) {
		lines.push_back(it->first + "=" + it->second);
	}
	return Utils::saveAll(this->filename(), lines);
}

void IniSettings::load()
{
	if (this->pIsLoaded) {
		return;
	}
	this->pIsLoaded = true;
	std::list<std::string> lines;
	if (!Utils::readAll(this->filename(), lines) || lines.empty()) {
		return;
	}
	std::list<std::string> tmp;
	std::list<std::string>::const_iterator it = lines.begin();
	for (; it != lines.end(); ++it) {
		Utils::split(*it, "=", tmp);
		if (tmp.size() == 2) {
			this->pStore[tmp.front()] = tmp.back();
		}
	}
}