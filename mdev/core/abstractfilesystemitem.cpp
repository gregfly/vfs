#include "abstractfilesystemitem.h"

AbstractFileSystemItem::AbstractFileSystemItem(const Type &type, AbstractFileSystem * fileSystem) : pType(type), pFileSystem(fileSystem)
{
	//
}

AbstractFileSystemItem::~AbstractFileSystemItem()
{
	//
}

const AbstractFileSystemItem::Type AbstractFileSystemItem::type() const
{
	return this->pType;
}

AbstractFileSystem * AbstractFileSystemItem::fileSystem() const
{
	return this->pFileSystem;
}