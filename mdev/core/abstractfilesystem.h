#ifndef ABSTRACTFILESYSTEM_H
#define ABSTRACTFILESYSTEM_H

#include "inc.h"

class AbstractDir;
class AbstractFile;

class AbstractFileSystem
{
public:
	explicit AbstractFileSystem();
	virtual ~AbstractFileSystem();

	virtual bool isChangesPerformed() = 0;

	virtual bool exists(const std::string &filename) const = 0;
	virtual const AbstractDir * cd(const std::string &dirname) const = 0;
	virtual const AbstractDir * md(const std::string &dirname) = 0;
	virtual bool rd(const std::string &dirname, bool recursive = false) = 0;
	virtual const AbstractFile * mf(const std::string &filename) = 0;
	virtual bool del(const std::string &filename) = 0;
	virtual bool lock(const std::string &filename, const std::string &key) = 0;
	virtual bool unlock(const std::string &filename, const std::string &key) = 0;
	virtual bool copy(const std::string &source, const std::string &destination) = 0;
	virtual bool move(const std::string &source, const std::string &destination) = 0;
	virtual void print(const std::string &dirname, std::list<std::string> &output) = 0;

	virtual bool isRelativePath(const std::string &path) const = 0;
	virtual std::string normalizePath(const std::string &path, bool * ok = NULL) const = 0;

	virtual std::string separator() const;
};

#endif