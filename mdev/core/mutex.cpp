#include "mutex.h"

#include "inc.h"
#include <pthread.h>

Mutex::Mutex() : pMutex(NULL), pIsLocked(false)
{
	//
}

Mutex::~Mutex()
{
	if (this->pMutex) {
		pthread_mutex_destroy(static_cast<pthread_mutex_t *>(this->pMutex));
		delete this->pMutex;
		this->pMutex = NULL;
	}
	this->pIsLocked = false;
}

bool Mutex::isLocked() const
{
	return this->pMutex && this->pIsLocked;
}

bool Mutex::lock()
{
	if (!this->pMutex) {
		pthread_mutex_t * mutex = new pthread_mutex_t();
		this->pMutex = mutex;
		pthread_mutex_init(mutex, NULL);
	}
	return this->pIsLocked = (pthread_mutex_lock(static_cast<pthread_mutex_t *>(this->pMutex)) == 0);
}

bool Mutex::unlock()
{
	if (!this->isLocked()) {
		return false;
	}
	return this->pIsLocked = !(pthread_mutex_unlock(static_cast<pthread_mutex_t *>(this->pMutex)) == 0);
}