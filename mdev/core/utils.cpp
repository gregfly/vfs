#include "utils.h"

#include "defs.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <regex>

Utils::Utils()
{
	//
}

int Utils::stringToInt(const std::string &string)
{
	if (string.length()) {
		return atoi(string.c_str());
	}
	return 0;
}

std::string Utils::intToString(int value)
{
	return std::to_string((_Longlong)value);
}

std::string Utils::join(const std::list<std::string> &parts, const std::string &delimiter)
{
	std::string string;
	std::list<std::string>::const_iterator it = parts.begin();
	for (; it != parts.end(); ++it) {
		if (it != parts.begin()) {
			string.append(delimiter);
		}
		string.append(*it);
	}
	return string;
}

void Utils::split(const std::string &string, const std::string &delimiter, std::list<std::string> &result)
{
	result.clear();
	int i = 0;
    int pos = string.find(delimiter);
    do {
		if (pos == std::string::npos) {
			result.push_back(string.substr(i, string.length()));
			break;
		} else {
			result.push_back(string.substr(i, pos-i));
			i = ++pos;
			pos = string.find(delimiter, pos);
		}
		if (pos == std::string::npos) {
			result.push_back(string.substr(i, string.length()));
		}
    } while (pos != std::string::npos);
}

int Utils::compare(const std::string &a, const std::string &b, bool ignoreCase)
{
	if (ignoreCase) {
		return stricmp(a.c_str(), b.c_str());
	}
	return strcmp(a.c_str(), b.c_str());
}

bool Utils::match(const std::string &subject, const std::string &pattern)
{
	return std::regex_match(subject, std::regex(pattern));
}

void Utils::print(const std::string &string)
{
	std::cout<<string;
}

void Utils::println(const std::string &string)
{
	std::cout<<string<<std::endl;
}

void Utils::pause()
{
	int i;
	std::cin>>i;
}

#ifdef APP_ENV_WINDOWS

#include <windows.h>

void Utils::sleep(long msec)
{
	Sleep(msec);
}

#else

#include <unistd.h>

void Utils::sleep(long msec)
{
	usleep(msec * 1000);
}

#endif

bool Utils::readAll(const std::string &filename, std::list<std::string> &output)
{
	output.clear();
	std::ifstream fileStream(filename, std::ifstream::in);
	if (!fileStream.is_open()) {
		return false;
	}
	std::string line;
	while (std::getline(fileStream, line)) {
		output.push_back(line);
	}
	fileStream.close();
	return true;
}

bool Utils::saveAll(const std::string &, const std::list<std::string> &)
{
	return false;
}