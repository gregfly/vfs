#include "dummyfile.h"

#include "abstractfilesystem.h"
#include "dummydir.h"
#include "utils.h"

DummyFile::DummyFile(const std::string &name, DummyDir * parent, AbstractFileSystem * fileSystem) : AbstractFile(fileSystem), pName(name), pParent(parent)
{
	//
}

DummyFile::DummyFile(const DummyFile &other) : AbstractFile(other.fileSystem()), pName(other.name()), pParent(other.parent())
{
	//
}

DummyFile::~DummyFile()
{
	this->pLockers.clear();
}

DummyDir * DummyFile::parent() const
{
	return this->pParent;
}

void DummyFile::setParent(DummyDir * value)
{
	this->pParent = value;
}

const std::string &DummyFile::name() const
{
	return this->pName;
}

void DummyFile::setName(const std::string &value)
{
	this->pName = value;
}

const std::string DummyFile::path() const
{
	std::list<std::string> parts;
	parts.push_front(this->pName);
	DummyDir * parent = this->pParent;
	while (parent) {
		parts.push_front(parent->name());
		parent = parent->parent();
	}
	return this->fileSystem()->normalizePath(Utils::join(parts, this->fileSystem()->separator()));
}

bool DummyFile::isLocked() const
{
	return !this->pLockers.empty();
}

void DummyFile::setLocked(const std::string &key, bool enable)
{
	this->pLockers.remove(key);
	if (enable) {
		this->pLockers.push_back(key);
	}
}

void DummyFile::print(const std::string &prefix, std::list<std::string> &output) const
{
	output.push_back(prefix + "_" + this->name() + (this->pLockers.empty()? "" : "[LOCKED by " + Utils::join(this->pLockers, ", ") + "]"));
}