#ifndef THREAD_H
#define THREAD_H

class Thread
{
public:
	explicit Thread();
	virtual ~Thread();

	bool isRunning() const;

	bool start();
	bool stop();
	void wait();

	void sleep(long msec);

protected:
	virtual void run() = 0;

private:
	void * pTid;
	bool pIsRunning;

	static void * internalRun(void *);

};

#endif