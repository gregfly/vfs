#ifndef MUTEX_H
#define MUTEX_H

class Mutex
{
public:
	explicit Mutex();
	virtual ~Mutex();

	bool isLocked() const;

	bool lock();
	bool unlock();

private:
	void * pMutex;
	bool pIsLocked;

};

#endif