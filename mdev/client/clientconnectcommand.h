#ifndef CLIENTCONNECTCOMMAND_H
#define CLIENTCONNECTCOMMAND_H

#include "../core/abstractcommand.h"

class ClientConnectCommand : public AbstractCommand
{
public:
	explicit ClientConnectCommand(const int &defaultPort);
	virtual ~ClientConnectCommand();

	int defaultPort() const;
	void setDefaultPort(const int &);

	virtual void resolveParams(const std::list<std::string> &tokens, std::map<std::string, std::string> &params) const;

	virtual void run(const std::map<std::string, std::string> &params, std::list<std::string> &output) const;

private:
	int pDefaultPort;

};

#endif