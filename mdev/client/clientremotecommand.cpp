#include "clientremotecommand.h"

#include "clientcommandexecutor.h"
#include "../net/requestdata.h"
#include "../net/responsedata.h"
#include "../core/exception.h"
#include <netlink\socket.h>

ClientRemoteCommand::ClientRemoteCommand() : AbstractCommand("remote")
{
	//
}

ClientRemoteCommand::~ClientRemoteCommand()
{
	//
}

void ClientRemoteCommand::resolveParams(const std::list<std::string> &tokens, std::map<std::string, std::string> &params) const
{
	// not need
}

void ClientRemoteCommand::run(const std::map<std::string, std::string> &params, std::list<std::string> &output) const
{
	ClientCommandExecutor * executor = dynamic_cast<ClientCommandExecutor *>(this->commandExecutor());
	if (!executor->socket()) {
		throw Exception("Connection is not active");
	}
	AbstractData * requestData = executor->requestData();
	NL::Socket * socket = executor->socket();

	try {
		requestData->setCommand(this->commandExecutor()->commandLine());

		socket->send(requestData->data(), requestData->size());

		ResponseData responseData;
		int status = socket->read(responseData.data(), responseData.size());
		if (status < 0) {
			executor->close();
			throw Exception("Server is not available");
		}
		std::list<std::string> outputData = responseData.outputList();
		std::list<std::string>::const_iterator it = outputData.begin();
		for (; it != outputData.end(); ++it) {
			output.push_back(*it);
		}
	} catch (const NL::Exception ex) {
		executor->close();
		throw Exception(ex.what());
	}
}