#ifndef CLIENTREMOTECOMMAND_H
#define CLIENTREMOTECOMMAND_H

#include "../core/abstractcommand.h"

class ClientRemoteCommand : public AbstractCommand
{
public:
	explicit ClientRemoteCommand();
	virtual ~ClientRemoteCommand();

	virtual void resolveParams(const std::list<std::string> &tokens, std::map<std::string, std::string> &params) const;

	virtual void run(const std::map<std::string, std::string> &params, std::list<std::string> &output) const;

};

#endif