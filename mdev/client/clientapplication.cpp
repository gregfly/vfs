#include "clientapplication.h"

#include "clientcommandexecutor.h"

ClientApplication::ClientApplication(int argc, char * argv[]) : Application(argc, argv), pCommandExecutor(NULL)
{
	//
}

ClientApplication::~ClientApplication()
{
	if (this->pCommandExecutor) {
		delete this->pCommandExecutor;
		this->pCommandExecutor = NULL;
	}
}

ClientCommandExecutor * ClientApplication::commandExecutor()
{
	if (!this->pCommandExecutor) {
		this->pCommandExecutor = new ClientCommandExecutor();
	}
	return this->pCommandExecutor;
}

int ClientApplication::exec()
{
	return 0;
}