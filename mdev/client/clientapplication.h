#ifndef CLIENTAPPLICATION_H
#define CLIENTAPPLICATION_H

#include "../core/application.h"

class ClientCommandExecutor;

class ClientApplication : public Application
{
public:
	explicit ClientApplication(int argc, char * argv[]);
	virtual ~ClientApplication();

	ClientCommandExecutor * commandExecutor();

	virtual int exec();

private:
	ClientCommandExecutor * pCommandExecutor;

};

#endif