#ifndef CLIENTCOMMANDEXECUTOR_H
#define CLIENTCOMMANDEXECUTOR_H

#include "../core/inc.h"
#include "../core/commandexecutor.h"

class AbstractData;

namespace NL {
	class Socket;
};

class ClientCommandExecutor : public CommandExecutor
{
public:
	explicit ClientCommandExecutor();
	virtual ~ClientCommandExecutor();

	NL::Socket * open(const std::string &host, int port);
	bool close();

	NL::Socket * socket() const;

	AbstractData * requestData() const;
	
protected:
	virtual AbstractCommand * resolveCommand(const std::string &);

	NL::Socket * pSocket;
	AbstractData * pRequestData;

};

#endif