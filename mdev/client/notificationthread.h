#ifndef NOTIFICATIONTHREAD_H
#define NOTIFICATIONTHREAD_H

#include "../core/thread.h"

namespace NL
{
	class Socket;
}

class Mutex;

class NotificationThread : public Thread
{
public:
	explicit NotificationThread(Mutex * mutex, const unsigned &timeout);
	virtual ~NotificationThread();

	NL::Socket * socket() const;
	void setSocket(NL::Socket *);

	unsigned timeout() const;
	void setTimeout(unsigned);

protected:
	void run();

private:
	Mutex * pMutex;
	NL::Socket * pSocket;
	unsigned pTimeout;

};

#endif