#ifndef CLIENTQUITCOMMAND_H
#define CLIENTQUITCOMMAND_H

#include "../core/abstractcommand.h"

class ClientQuitCommand : public AbstractCommand
{
public:
	explicit ClientQuitCommand();
	virtual ~ClientQuitCommand();

	virtual void run(const std::map<std::string, std::string> &params, std::list<std::string> &output) const;

};

#endif