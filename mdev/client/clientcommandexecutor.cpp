#include "clientcommandexecutor.h"

#include "../net/requestdata.h"
#include "../core/exception.h"
#include <netlink/socket.h>

ClientCommandExecutor::ClientCommandExecutor() : CommandExecutor(), pSocket(NULL), pRequestData(NULL)
{
	//
}

ClientCommandExecutor::~ClientCommandExecutor()
{
	if (this->pSocket) {
		delete this->pSocket;
		this->pSocket = NULL;
	}
	if (this->pRequestData) {
		delete this->pRequestData;
		this->pRequestData = NULL;
	}
}

NL::Socket * ClientCommandExecutor::open(const std::string &host, int port)
{
	if (this->pSocket || this->pRequestData) {
		this->close();
	}
	NL::init();
	this->pSocket = new NL::Socket(host, port);
	this->pRequestData = new RequestData();
	return this->pSocket;
}

bool ClientCommandExecutor::close()
{
	if (this->pRequestData) {
		delete this->pRequestData;
		this->pRequestData = NULL;
	}
	if (this->pSocket) {
		delete this->pSocket;
		this->pSocket = NULL;
		return true;
	}
	return false;
}

NL::Socket * ClientCommandExecutor::socket() const
{
	return this->pSocket;
}

AbstractData * ClientCommandExecutor::requestData() const
{
	assert(this->pRequestData != NULL);
	return this->pRequestData;
}

AbstractCommand * ClientCommandExecutor::resolveCommand(const std::string &name)
{
	try {
		return CommandExecutor::resolveCommand(name);
	} catch (const Exception &ex) {
		//
	}
	try {
		return CommandExecutor::resolveCommand("remote");
	} catch (const Exception &ex) {
		//
	}
	throw Exception("Command " + name + " not found...");
}