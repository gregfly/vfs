#include "clientconnectcommand.h"

#include "clientcommandexecutor.h"
#include "../net/responsedata.h"
#include "../core/utils.h"
#include "../core/exception.h"
#include <netlink/socket.h>

ClientConnectCommand::ClientConnectCommand(const int &defaultPort) : AbstractCommand("connect"), pDefaultPort(defaultPort)
{
	//
}

ClientConnectCommand::~ClientConnectCommand()
{
	//
}

int ClientConnectCommand::defaultPort() const
{
	return this->pDefaultPort;
}

void ClientConnectCommand::setDefaultPort(const int &value)
{
	this->pDefaultPort = value;
}

void ClientConnectCommand::resolveParams(const std::list<std::string> &tokens, std::map<std::string, std::string> &params) const
{
	if (tokens.size() != 3) {
		throw Exception("Using: " + this->id() + " host[:port] username");
	}
	std::list<std::string>::const_iterator it = tokens.begin();
	it++;
	if (!Utils::match(*it, "([a-zA-Z_0-9.:]+)?")) {
		throw Exception("Using: " + this->id() + " host[:port] username");
	}
	std::list<std::string> parts;
	const std::string &host = *it;
	Utils::split(host, ":", parts);
	params["host"] = parts.front();
	params["port"] = parts.size() > 1? parts.back() : Utils::intToString(this->defaultPort());
	++it;
	params["username"] = *it;
}

void ClientConnectCommand::run(const std::map<std::string, std::string> &params, std::list<std::string> &output) const
{
	ClientCommandExecutor * executor = dynamic_cast<ClientCommandExecutor *>(this->commandExecutor());
	/*if (executor->socket()) {
		throw Exception("You already connected to server");
	}*/

	try {
		NL::Socket * socket = executor->open(params.at("host"), Utils::stringToInt(params.at("port")));
		AbstractData * requestData = executor->requestData();

		requestData->setUsername(params.at("username"));
		requestData->setPassword("");
		requestData->setCommand(this->commandExecutor()->commandLine());

		socket->send(requestData->data(), requestData->size());

		ResponseData responseData;
		if (socket->read(responseData.data(), responseData.size()) < 0) {
			throw Exception("Server is not available");
		}
		std::list<std::string> outputData = responseData.outputList();
		std::list<std::string>::const_iterator it = outputData.begin();
		for (; it != outputData.end(); ++it) {
			output.push_back(*it);
		}
		if (!responseData.isSuccess()) {
			throw Exception("Auth failed");
		}
		requestData->setPassword(responseData.password());
	} catch (const NL::Exception ex) {
		executor->close();
		throw Exception(ex.what());
	} catch (const Exception ex) {
		executor->close();
		throw ex;
	}
	output.push_back("Success " + params.at("host") + " : " + params.at("username"));
}