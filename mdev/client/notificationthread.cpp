#include "notificationthread.h"

#include "../core/mutex.h"
#include "../core/utils.h"
#include "../net/notificationdata.h"
#include <netlink/socket.h>

NotificationThread::NotificationThread(Mutex * mutex, const unsigned &timeout) : Thread(), pMutex(mutex), pSocket(NULL), pTimeout(timeout)
{
	assert(mutex != NULL);
}

NotificationThread::~NotificationThread()
{
	//
}

NL::Socket * NotificationThread::socket() const
{
	return this->pSocket;
}

void NotificationThread::setSocket(NL::Socket * socket)
{
	this->pSocket = socket;
}

unsigned NotificationThread::timeout() const
{
	return this->pTimeout;
}

void NotificationThread::setTimeout(unsigned value)
{
	this->pTimeout = value;
}

void NotificationThread::run()
{
	try {
		while (this->pSocket) {
			this->pMutex->lock();
				int socketHandler = this->pSocket->socketHandler();
				unsigned long long finTime = NL::getTime() + this->pTimeout;
				fd_set setSockets;
				FD_ZERO(&setSockets);
				FD_SET(socketHandler, &setSockets);

				unsigned long long milisecLeft = finTime - NL::getTime();
				struct timeval timeout;

				timeout.tv_sec = milisecLeft / 1000;
				timeout.tv_usec = (milisecLeft % 1000) * 1000;

				int status = select(socketHandler + 1, &setSockets, NULL, NULL, &timeout);
				if (status > 0) {
					NotificationData notificationData;
					this->pSocket->read(notificationData.data(), notificationData.size());
					Utils::println(notificationData.username() + " performs command: " + notificationData.command());
				}
			this->pMutex->unlock();
			this->sleep(10);
		}
	} catch (const NL::Exception &) {
		Utils::println("Server disconnected...");
		this->pMutex->unlock();
	}
}