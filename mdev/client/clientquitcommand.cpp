#include "clientquitcommand.h"

#include "../core/abstractcommandexecutor.h"

ClientQuitCommand::ClientQuitCommand() : AbstractCommand("quit")
{
	//
}

ClientQuitCommand::~ClientQuitCommand()
{
	//
}

void ClientQuitCommand::run(const std::map<std::string, std::string> &params, std::list<std::string> &output) const
{
	output.push_back("bay");
	this->commandExecutor()->setRunning(false);
}